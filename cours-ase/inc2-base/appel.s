f:  sub  1,%sp		// laisse de la place pour y

    move (%sp+3),%a	// a $\leftarrow$ argument x
    add  2,%a		// a $\leftarrow$ a+2
    push %a		// empiler x+2 (v)
    push 10		// empiler 10 (u)
    call g
    add  2,%sp		// dépiler les 2 arguments
    move %a,(%sp+1)	// y $\leftarrow$ résultat de g (dans a)

    move (%sp+1),%a	// a $\leftarrow$ y (pourrait être optimisé)
    add  20,%a		// a $\leftarrow$ y+20

    add  1,%sp		// fin d'existence pour y
    rtn			// résultat dans a

g:  move (%sp+2),%a	// a $\leftarrow$ u

    push %b		// on va utiliser b, on le sauvegarde
    move (%sp+4),%b	// b $\leftarrow$ v
    add  %b,%a		// a $\leftarrow$ a+b = u+v
    pop  %b		// restaurer b

    rtn			// résultat dans a
