#define KBD_REG_STATUS		20
#define KBD_REG_DATA		21

#def	KBD_STA_KEY_PRESSED	1		// codes lus dans le registre d'état
#def	KBD_STA_KEY_RELEASED	2		// cf doc du contrôleur

lire_clavier:
// attendre qu'une touche soit appuyée : boucler tant que statut $\neq$ 1
	in   (KBD_REG_STATUS),%a		// a $\leftarrow$ statut du contrôleur
	test %a,KBD_STA_KEY_PRESSED		// tester a par rapport à 1
	jne  lire_clavier			// boucle si \%a $\neq$ 1
// lire la touche
	in   (KBD_REG_DATA),%a			// a $\leftarrow$ code de la touche
	rtn
