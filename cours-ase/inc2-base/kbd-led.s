#define KBD_REG_CTRL		KBD_REG_STATUS	// même numéro de port
#define	KBD_CTRL_LED_ON		1		// codes extraits de la doc
#define	KBD_CTRL_LED_OFF	2
#define	KBD_LED_NUMLOCK		12		// numéros des LED du clavier
#define	KBD_LED_CAPSLOCK	13

// allumer la LED "Caps Lock"
allumer_caps_lock:
	out KBD_LED_CAPSLOCK,(KBD_REG_DATA)	// numéro de la LED à allumer
	out KBD_CTRL_LED_ON,(KBD_REG_CTRL)	// envoyer l'ordre d'allumer la LED
	rtn
