    // void swtch (struct proc *elu)
    swtch:
	// sauvegarder les registres actuels dans l'ancienne pile
	// (PC est déjà sauvegardé : adresse de retour de swtch)
250	push %a
251	push %b
252	push %sr
	// sauvegarder sp dans l'ancien descripteur de processus
253	move (1000),%a		// a $\leftarrow$ curproc
254	move %sp,(%a+5)		// curproc->kstack $\leftarrow$ sp

	// changer curproc : curproc $\leftarrow$ argument 1 = \texttt{elu}
255	move (%sp+5),%a		// a $\leftarrow$ elu
256	move %a,(1000)		// curproc $\leftarrow$ a = elu

	// changer de pile noyau
257	move (%a+5),%sp		// pile noyau du nouveau processus

	// restaurer les registres du nouveau processus
258	pop  %sr
259	pop  %b
260	pop  %a

261	rtn			// retour à l'appelant de swtch pour le nouveau processus
