int sys_open (void)			// fonction appelée par traiter\_trap
{
    int *sp ;
    char *path ; int mode, droits ;	// arguments de open

    sp = curproc->sp ;			// valeur du registre SP (pile utilisateur) sauvegardée
    path = *(sp+1) ;			// premier argument (oui, on prend des libertés avec les types...)
    mode = *(sp+2) ;			// deuxième argument
    droits = *(sp+3) ;			// troisième argument

    // reste du code de la primitive
    ...
}
