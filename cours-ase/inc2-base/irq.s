	interruption:			// étiquette placée à l'adresse 200
	// sauvegarder b dans l'espace temporaire (tmp) situé à partir de l'adresse 100
200		move %b,(103)		// tmp[3] $\leftarrow$ b
	// chercher l'adresse du descripteur du processus courant :
	// on suppose que la variable à l'adresse 1000 (= curproc) contient son adresse
201		move (1000),%b		// b $\leftarrow$ contenu de la case 1000 (= curproc)
202		move %a,(%b)		// *curproc = curproc[0] $\leftarrow$ a
	// sauvegarde de b
203		move (103),%a		// restaurer la valeur de b sauvegardée en 200
204		move %a,(%b+1)		// curproc[1] $\leftarrow$ tmp[3] (= b)
	// sauvegarde de pc
205		move (100),%a
206		move %a,(%b+2)		// curproc[2] $\leftarrow$ tmp[0] (= pc)
	// sauvegarde de sr
207		move (101),%a
208		move %a,(%b+3)		// curproc[3] $\leftarrow$ tmp[1] (= sr)
	// sauvegarde de sp
209		move %sp,(%b+4)		// curproc[4] $\leftarrow$ sp

	// on suppose que le descripteur du processus contient l'adresse de la pile noyau du processus
210		move (%b+5),sp		// sp $\leftarrow$ curproc[5]

	// appeler la fonction pour réaliser la suite des traitements
211		call lasuite
