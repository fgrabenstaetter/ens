int sys_read (void)			// fonction appelée par traiter\_trap
{
    char *buf ; int fd, n ;		// arguments de read

    // récupération des arguments
    ...

    // boucle sur n (tant qu'il y a des octets à lire)
    while (n > 0) {
	...
	if (pas_de_donnee_a_lire) {	// aie aie aie...
	    struct proc *elu ;		// nouveau processus
	    elu = processus_pret () ;	// choisir un processus prêt à tourner
	    swtch (elu) ;		// \textbf{changer pour le processus choisi}
	    // lorsqu'on arrive ici, c'est qu'on est remis sur le processeur
	}
    }

    // reste du code de la primitive
    ...
}
