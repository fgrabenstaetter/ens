#define NENTIERS 100

void somme (void)
{
    int fd, *tab ;

    fd = open ("toto", O_RDWR) ;
    tab = mmap (NULL, sizeof (int) * NENTIERS,
		    PROT_READ | PROT_WRITE,	// accès R+W
		    MAP_SHARED,			// modifier fichier
		    fd, 0) ;			// tout le fichier
    tab [0] = 0 ;			// écriture dans le fichier
    for (int i = 1 ; i < NENTIERS ; i++)
	tab [0] += tab [i] ;		// lecture + écriture dans le fichier
    munmap (tab, sizeof (int) * NENTIERS) ;
    close (fd) ;
}
