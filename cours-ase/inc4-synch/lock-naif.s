verrouiller:			// argument dans la pile
	move (%sp+2),%a		// a $\leftarrow$ adresse du verrou
boucle:
	move (%a),%b		// b $\leftarrow$ *verrou
        test %b			// teste la valeur de b
	jnz  boucle		// boucle si b $\neq$ 0
        move 1,%b
	move %b,(%a)		// *verrou $\leftarrow$ 1
        rtn
