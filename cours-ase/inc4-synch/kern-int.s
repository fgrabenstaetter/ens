demarrer_requete:
    move (%sp+2),%a	// le descripteur de la requête
    ...			// envoyer le numéro de secteur à lire
    out  ...,(50)	// démarrer la lecture
    rtn

masquer_interruptions:	 // retirer le bit IE de SR
    and  0xffef,%sr	 // sr $\leftarrow$ sr AND tous les bits à 1 sauf IE
    rtn

demasquer_interruptions: // ajouter le bit IE à SR
    or   0x0010,%sr	 // sr $\leftarrow$ sr OR bit IE à 1
    rtn
