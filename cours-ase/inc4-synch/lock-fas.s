verrouiller:			// argument dans la pile
	move (%sp+2),%a		// a $\leftarrow$ adresse du verrou
boucle:
	fas  (%a),%b		// b $\leftarrow$ *verrou ET *verrou $\leftarrow$ 1
        test %b			// teste la valeur de b
	jnz  boucle		// boucle si b $\neq$ 0
        rtn
