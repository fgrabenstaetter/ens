\incdir {inc1-intro}

\titreA {Introduction}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pourquoi étudier les systèmes d'exploitation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Introduction}

\begin {frame} {Motivations}
    Intérêt de comprendre le fonctionnement des systèmes
    d'exploitation :

    \begin {itemize}
	\item sur ordinateur/téléphone/tablette/etc, c'est le
	    programme :
	    \begin {itemize}
		\item le plus utilisé
		\item le moins visible
		\item parmi les plus complexes
	    \end {itemize}

	\item permet de mieux utiliser le système

	\item permet d'en comprendre les principaux paramètres

	\item certains points abordés (ex : concurrence) sont
	    utiles en programmation

	\item l'un des chaînons manquants entre l'architecture
	    matérielle et la programmation
	    \begin {itemize}
		\item l'autre est le compilateur
	    \end {itemize}

	\item c'est passionnant !

    \end {itemize}
\end {frame}

\begin {frame} {Bibliographie}
    \begin {itemize}
	\item \textit{Operating System Concepts} (toutes éditions) \\
	    A. Silberschatz, P. Galvin et G. Gagne \\
	    Ed. John Wiley \& Sons
	\item \textit{Operating Systems: Three Easy Pieces} \\
	    R. Arpaci-Dusseau et A. Arpaci-Dusseau \\
	    Disponible sur \url{http://pages.cs.wisc.edu/~remzi/OSTEP/}
	\item \textit{Operating Systems: Internals and Design Principles} \\
	    W. Stallings \\
	    Ed. Pearsons
	\item \textit{The Little Book of Semaphores} \\
	    A. Downey \\
	    Disponible sur \url{http://greenteapress.com/wp/semaphores/}
    \end {itemize}

\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Qu'est-ce qu'un système d'exploitation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Qu'est-ce qu'un système d'exploitation}

\begin {frame} {Qu'est-ce qu'un système d'exploitation ?}
    Comment définir ce qu'est un système d'exploitation (SE) ?

    \begin {itemize}
	\item Facile, m'sieur : Windows, Linux, FreeBSD, etc. !
    \end {itemize}

    \medskip

    Est-ce aussi simple~?

    \begin {itemize}
	\item si Linux est un SE, que sont Debian, Ubuntu, etc. ?
	\item et Android, iOS ?
	\item et QNX, RTLinux, VxWorks ?
	\item et Contiki, TinyOS ?
    \end {itemize}

    Et, au fait...

    \begin {itemize}
	\item est-ce qu'une box d'accès à l'Internet a un SE ?
	\item est-ce qu'un terminal X a un SE ?
	\item est-ce qu'une montre connectée a un SE ?
	\item est-ce qu'un thermostat de radiateur a un SE ?
	\item est-ce qu'une voiture a un SE ?
    \end {itemize}

\end {frame}

\begin {frame} {Rappel}
    Petit historique des systèmes d'exploitation :
    \begin {itemize}
	\item cf cours de programmation système \\
	    \url{https://gitlab.com/pdagog/ens/}
    \end {itemize}

    \medskip

    Les systèmes d'exploitation ont été créés en réponse à des besoins :
    \begin {itemize}
	\item rentrer des programmes dans la mémoire de l'ordinateur
	    \begin {itemize}
		\item gestion des périphériques (exemple : lecteur de
		    cartes perforées)
		\item enchaînement des \emph{jobs} \implique moniteur,
		    traitement \emph{batch}

	    \end {itemize}

	\item rentabiliser le temps (coûteux) de l'ordinateur
	    \begin {itemize}
		\item exploiter le parallélisme entre processeur et
		    entrées/sorties
		\item \implique mécanisme des interruptions matérielles
	    \end {itemize}

	\item utilisation interactive des ordinateurs
	    \begin {itemize}
		\item terminaux, Teletype
		\item partage du temps de l'ordinateur entre programmes (tâches)
		\item \implique interruption d'horloge
		\item isolation des tâches entre elles
	    \end {itemize}

    \end {itemize}

\end {frame}

\begin {frame} {Qu'est-ce qu'un système d'exploitation}
    Qu'entend-on aujourd'hui par \frquote{système d'exploitation} ?

    \begin {itemize}
	\item pour certains : \frquote{système d'exploitation} = tout ce
	    qui est livré sur le DVD ou l'image téléchargée

	    \begin {itemize}
		\item noyau, utilitaires, interface graphique, jeu du
		    solitaire, etc.

	    \end {itemize}

    \end {itemize}

    \bigskip

    Il suffit de voir quelques exemples de \frquote{documentation}...

\end {frame}

\begin {frame} {Exemple : Unix / Linux}
    \begin {center}
	\fig {arch-linux} {.8}
    \end {center}
\end {frame}


\begin {frame} {Exemple : Windows 2000}
    \begin {center}
	\fig {arch-w2k} {.5}
	\\
	\credit{Crédit grm -- Wikipedia \ccbysa}
    \end {center}
\end {frame}

\begin {frame} {Exemple : Android}
    \begin {center}
	\fig {arch-android} {.8}
	\\
	\credit{D'après \url{https://developer.android.com/guide/platform/}}
    \end {center}
\end {frame}

\begin {frame} {Exemple : iOS / MacOS X}
    \begin {center}
	\fig {arch-ios} {.8}
    \end {center}
\end {frame}

\begin {frame} {Qu'est-ce qu'un système d'exploitation}
    \begin {itemize}
	\item apport majeur d'Unix : séparation du \alert{noyau} et
	    des programmes
	    \begin {itemize}
		\item seule partie exécutée en \alert{mode
		    superviseur} (voir plus loin)

	    \end {itemize}

	\item distinction nette avec Linux :
	    \begin {itemize}
		\item Linus Torvalds crée et maintient le
		    \alert{noyau}

		\item des organisations (commerciales ou non)
		    assemblent le noyau et des programmes pour créer
		    des \alert{distributions} : RedHat, Debian,
		    Ubuntu, etc.
	    \end {itemize}
    \end {itemize}
\end {frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Le noyau
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Le noyau}

\begin {frame} {Le noyau -- Objectifs fondamentaux}
    Le noyau a deux objectifs fondamentaux~:
    \begin {itemize}
	\item \textbf {partager équitablement les ressources}
	\item \textbf {garantir la sécurité des données}
    \end {itemize}

    \bigskip

    ... tout en restant efficace (\emph{overhead} minimum)

\end {frame}

\begin {frame} {Le noyau}
    Partager équitablement les ressources~:

    \begin {itemize}
	\item mémoire vive
	\item temps processeur
	\item carte réseau
	\item espace disque
	\item accès aux disques
	\item etc.
    \end {itemize}
\end {frame}

\begin {frame} {Le noyau}
    Garantir la sécurité des données

    \begin {itemize}
	\item un processus ne doit pas accéder aux données d'un autre
	    processus (sauf si autorisé)

	\item un utilisateur ne doit pas accéder à un fichier non autorisé

	\item un utilisateur ne doit pas terminer un processus d'un
	    autre utilisateur

	\item etc.

    \end {itemize}
\end {frame}

\begin {frame} {Le noyau}

    Pour atteindre les deux objectifs fondamentaux :

    \begin {itemize}
	\item le \textbf {noyau} s'exécute en \textbf {mode privilégié}
	    \\
	    \implique il a accès à toutes les ressources matérielles

	\item les programmes (applications) s'exécutent, dans le
	    contexte de \textbf {processus}, en \textbf {mode non
	    privilégié}
	    \\
	    \implique appels au noyau pour exécuter certaines opérations

    \end {itemize}

    \bigskip

    Les appels au noyau sont les \textbf {primitives systèmes}

    \begin {itemize}
	\item Interface de programmation du noyau
	    \\
	    \textit {Application Programming Interface} (API)
	\item Forme : appels de fonction en C
	\item Exemple :
	    \\
	    \code{int open (const char *path, int flag, mode\_t mode)}
	\item Norme POSIX (ISO/IEC 9945)
    \end {itemize}


\end {frame}

\begin {frame} {Le noyau}
    \begin {center}
	\fig {noyau} {.7}
    \end {center}
\end {frame}

\begin {frame} {Le noyau}
    Définition :

    \begin {quote}
	Le noyau est constitué de l'ensemble minimum des primitives
	systèmes nécessaires pour atteindre les deux objectifs
	fondamentaux

    \end {quote}

\end {frame}

\begin {frame} {Le noyau}
    \begin {itemize}
	\item Démarrage de l'ordinateur : noyau installé
	    en mémoire \\
	    \implique il y restera jusqu'à la fin (redémarrage,
	    arrêt, ou crash)

	\item Le noyau s'exécute en mode privilégié

	    \begin {itemize}
		\item code sensible
		\item difficile à développer, à mettre au point
		\item plus il est petit, mieux c'est
		\item tout ce qui peut être mis ailleurs doit l'être
		    \\
		    \vspace* {0.8mm}
		    Exemple :
		    \begin {itemize}
			\item pour le noyau, un utilisateur = un nombre
			    entier
			\item \code{ls -l} affiche des noms de login
			\item \implique la conversion
			    \frquote{entier $\leftrightarrow$ nom} est
			    effectuée par \code{ls}

		    \end {itemize}
	    \end {itemize}

    \end {itemize}
\end {frame}

\begin {frame} {Le noyau -- Conséquences des deux objectifs}
    Des deux objectifs fondamentaux découlent des \textbf{propriétés}
    subséquentes :

    \begin {itemize}
	\item un programme d'un utilisateur ne doit pas avoir un
	    accès intégral à l'ordinateur...
	    \\
	    sinon il pourrait aller consulter n'importe quel
	    secteur sur le disque ou altérer la mémoire d'un autre
	    processus ou...
	\item il faut donc avoir un accès \alert{contrôlé} aux
	    ressources matérielles
	    \\
	    \implique notion d'\textbf{appel système}
	\item les ressources matérielles doivent être
	    \alert{abstraites} pour être contrôlées
	    \\
	    \implique couche d'\textbf{abstraction des périphériques}
	\item peut-on donner des droits à \alert{chaque secteur} du disque ?
	    \\
	    \implique \textbf{notion de fichier} (et de système de fichiers)

	\item il faut pouvoir désigner les \alert{titulaires} des droits
	    \\
	    \implique notion d'\textbf{utilisateur}

    \end {itemize}
\end {frame}

\begin {frame} {Le noyau -- Conséquences des deux objectifs}
    \begin {itemize}
	\item partage des ressources (et notamment du processeur)
	    \\
	    \implique notion d'\textbf{ordinateur virtuel} : chaque
	    processus \emph{croit} avoir l'ordinateur pour lui seul

	\item un processus \alert{ne doit pas avoir accès} à la mémoire d'un
	    autre processus s'il n'est pas autorisé
	    \\
	    \implique \textbf{isolation}/\textbf{étanchéité} des
	    processus entre eux

	\item l'isolation des processus les \alert{empêche de communiquer}
	    \\
	    \implique mécanismes de \textbf{communication inter-processus}

    \end {itemize}

    \bigskip

    Ne pas confondre les causes (les 2 objectifs fondamentaux) et
    les conséquences

\end {frame}

\begin {frame} {Le noyau}
    Le noyau est chargé en mémoire au démarrage de l'ordinateur
    \begin {itemize}
	\item seul programme à utiliser le mode \frquote{privilégié}
	\item initialise le processeur, la mémoire, les périphériques
	\item \frquote{répond} aux requêtes des processus \\
	    \implique le noyau implémente les primitives système
    \end {itemize}

    \bigskip

    Le noyau est un programme autonome :
    \begin {itemize}
	\item pas d'utilisation de bibliothèques externes
	\item édition de liens \implique un binaire unique et autonome
	\item ajout dynamique de modules
	    \begin {itemize}
		\item ajout de nouvelles fonctions
		    \begin {itemize}
			\item pilotes, systèmes de fichiers, etc.
			\item mise à jour de tables (ex: table des pilotes)
		    \end {itemize}
		\item édition de liens complémentaire au chargement
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Le noyau}
    Trois \frquote{accès} possibles au noyau :

    \medskip

    \begin {minipage} [c] {.35\linewidth}
	\fig {acces} {1}
    \end {minipage}
    \hfill
    \begin {minipage} [c] {.64\linewidth}
	% \fC
	\begin {itemize}
	    \item interruption matérielle
		\begin {itemize}
		    \item générée par un périphérique \\
			exemple : fin d'entrée/sortie
		    \item générée par l'horloge \\
			exemple : fin de quantum
		\end {itemize}
	    \item exception provoquée par le processus
		\begin {itemize}
		    \item violation de segment
		    \item division par 0
		    \item etc.
		\end {itemize}
	    \item appel d'une primitive système
		\begin {itemize}
		    \item instruction spéciale du processeur
		    \item considéré comme une exception
		\end {itemize}
	\end {itemize}
    \end {minipage}

    \bigskip

    Traitement similaire des 3 types d'accès
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Support matériel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Support matériel}

\begin {frame} {Support matériel}
    Support matériel indispensable pour assumer le rôle de gardien
    des ressources :

    \begin {enumerate}
	\item processeur avec (minimum) deux modes d'exécution

	    \begin {itemize}
		\item l'accès direct aux ressources est réservé au seul
		    noyau
		\item sinon : n'importe quel programme peut accéder
		    aux ressources sans contrôle
	    \end {itemize}

	\item mécanisme d'interruptions matérielles

	    \begin {itemize}
		\item permet de bénéficier du parallélisme des
		    périphériques
		\item sinon : noyau doit surveiller l'achèvement
		    des requêtes

	    \end {itemize}

	\item horloge capable d'interrompre périodiquement le processeur

	    \begin {itemize}
		\item permet au noyau de reprendre le contrôle
		\item sinon : système non préemptif
	    \end {itemize}

	\item mécanisme de traduction/protection mémoire

	    \begin {itemize}
		\item assure l'indépendance des espaces mémoire
		\item sinon : pas de protection des processus les uns par
		    rapport aux autres
	    \end {itemize}

    \end {enumerate}

\end {frame}
