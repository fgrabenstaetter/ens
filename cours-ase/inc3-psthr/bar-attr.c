pthread_barrier_t b ;
pthread_barrierattr_t ba ;

errno = pthread_barrierattr_init (&ba) ;
if (errno != 0)
    raler ("pthread_barrierattr_init") ;

errno = pthread_barrierattr_setpshared (&ba,
                           PTHREAD_PROCESS_SHARED) ;
if (errno != 0)
    raler ("pthread_barrierattr_setpshared") ;

errno = pthread_barrier_init (&b, &ba, 4) ;
if (errno != 0)
    raler ("pthread_barrier_init") ;

pthread_barrierattr_destroy (&ba) ;
