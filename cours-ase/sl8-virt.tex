\incdir {inc8-virt}

\titreA {Machines virtuelles}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Introduction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Introduction}

\begin {frame} {Introduction}
    Virtualisation : création d'un ordinateur virtuel simulant le
    comportement d'un ordinateur réel

    \bigskip

    \fig {screenshot} {1}
\end {frame}

\begin {frame} {Introduction -- Terminologie}

    \begin {itemize}
	\item \alert{machine physique} : l'ordinateur réel

	\item \alert{machine virtuelle} : l'environnement simulant
	    le comportement d'une  machine physique

	    \begin {itemize}
		\item processeur(s)
		\item périphérie
	    \end {itemize}

	\item \alert{\emph{Virtual Machine Monitor}} (VMM) : gestionnaire
	    de machines virtuelles, 

	    \begin {itemize}
		\item gère les machines virtuelles

		\item gère l'exécution de programmes dans une ou
		    plusieurs machines virtuelles

	    \end {itemize}

	\item \alert{hyperviseur} : partie du VMM dédiée à l'exécution
	    dans les machines virtuelles

	\item \alert{système hôte} : le système d'exploitation tournant
	    nativement sur la machine physique

	\item \alert{système invité} : le système d'exploitation
	    tournant dans l'environnement de la machine virtuelle

    \end {itemize}

\end {frame}

\begin {frame} {Introduction -- Historique}
    Historique :
    \begin {itemize}
	\item années 1960 : prototypes
	\item 1972 : VM370 d'IBM
	    \begin {itemize}
		\item premier système commercial de virtualisation
		\item base des \frquote{\emph{mainframes}} d'IBM
		\item plusieurs systèmes d'exploitation (IBM) l'utilisent
		\item \frquote{minidisk} : partie d'un disque dédiée
		    à un système invité
	    \end {itemize}
	\item 1994 : Bochs (émulation)
	\item 1999 : VMWare pour x86 (virtualisation)
	\item 2003 : Xen (paravirtualisation)
	\item 2006 : extensions AMD-V et Intel VT-x
	\item 2007 : kvm pour Linux, VirtualBox (virtualisation)
	\item 2008 : Intel Extended Page Tables, Hyper-V
	\item depuis les années 2010 : généralisation de la virtualisation

    \end {itemize}
\end {frame}

\begin {frame} {Introduction -- Motivations}
    Intérêts de la virtualisation :
    \begin {itemize}
	\item utilisation de plusieurs systèmes sur le même ordinateur
	    \begin {itemize}
		\item ex : machine virtuelle Windows avec un système
		    Unix
	    \end {itemize}
	\item rentabiliser l'utilisation du matériel
	    \begin {itemize}
		\item baisse des prix et augmentation de la puissance
		    des serveurs
		\item protéger les services en les isolant
		    sur des systèmes distincts
	    \end {itemize}
	\item gagner en souplesse
	    \begin {itemize}
		\item facilité et rapidité de déploiement
		\item allocation dynamique de ressources
		\item \frquote{horizontal scalability}
		\item facilité de gestion
	    \end {itemize}
	\item augmenter la résilience des infrastructures
	    \begin {itemize}
		\item souplesse \implique augmentation de la disponibilité
		\item migration facilitée de machine virtuelle
		\item PCA (Plan de Continuité d'Activité) / PRA (Plan de Reprise...)
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Introduction -- Motivations}
    Émergence de nouveaux concepts, métiers, et mots marketing :
    \begin {itemize}
	\item IaaS (\emph{Infrastructure as a Service\/}) :
		capacité pour un client d'obtenir des ressources
		    informatiques sur lesquelles il peut déployer,
		    exécuter et contrôler n'importe quel logiciel,
		    du système d'exploitation jusqu'aux applications.
	\item Cloud (public, privé)
	    \begin {itemize}
		\item cloud public : nombreuses offres commerciales
		    \begin {itemize}
			\item 2006 : Amazon EC2 (Elastic Cloud)
			\item 2008 : Google App Engine
			\item 2010 : OVH, Microsoft Azure
		    \end {itemize}
		\item cloud privé : logiciels de gestion de cloud
		    \begin {itemize}
			\item OpenNebula, OpenStack, Kubernetes, etc.
		    \end {itemize}
	    \end {itemize}
	\item métier \emph{DevOps}
	    \begin {itemize}
		\item devops = développeur + ingénieur système
		\item objectifs antagonistes \implique réduire le fossé
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Introduction -- Motivations}
    Aujourd'hui, la virtualisation est utilisée en masse par la plupart
    des organisations

    \bigskip

    Exemple : Direction du Numérique de l'Université de Strasbourg

    \begin {itemize}
	\item environ 150 serveurs physiques...
	\item hébergeant environ 900 serveurs virtuels !
	\item soit une moyenne de 6 machines virtuelles par serveur
	    physique
	\item en réalité : ratio d'environ 30 à 40
	    \begin {itemize}
		\item tous les serveurs ne sont pas virtualisés
		\item applications commerciales non supportées par
		    le vendeur

	    \end {itemize}
    \end {itemize}

\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Classification
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Classification}

\begin {frame} {Classification}
    Beaucoup de termes autour de la virtualisation :

    \begin {itemize}
	\item hyperviseur, paravirtualisation, type 2, etc.
    \end {itemize}

    \bigskip

    Comment s'y retrouver ?

    \begin {itemize}
	\item critères pour la virtualisation (Popek et Goldberg)
	\item classification (tentative de...)
	    \begin {itemize}
		\item position de l'hyperviseur
		\item modification du système virtuel
		\item environnement hôte
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Caractéristiques d'un environnement virtuel}
    Popek et Goldberg (1974) : 3 critères
    nécessaires pour la virtualisation

    \begin {itemize}
	\item \alert{fidélité} : l'environnement virtuel doit être
	    quasiment identique à la machine originale

	    \begin {itemize}
		\item un programme dans l'environnement virtuel doit
		    produire les mêmes effets que dans la machine
		    originale

	    \end {itemize}

	\item \alert{performances} : un programme dans l'environnement
	    virtuel ne doit subir qu'une dégradation mineure de ses
	    performances

	\item \alert{sécurité} : le gestionnaire de machine virtuelle
	    a le contrôle total des ressources
	    \begin {itemize}
		\item il n'est pas possible pour un programme dans
		    l'environnement virtualisé d'accéder à des
		    ressources non allouées

		\item le gestionnaire de machine virtuelle peut reprendre
		    le contrôle de ressources déjà allouées

	    \end {itemize}

    \end {itemize}

    \implique dans la pratique, certaines technologies de virtualisation
    ne respectent pas ces 3 critères
\end {frame}

\begin {frame} {Gestionnaire de machine virtuelle}
    Fonctions d'un gestionnaire de machine virtuelle :

    \begin {itemize}
	\item gérer les machines virtuelles (création, lancement, etc.)
	\item gérer l'exécution des machines virtuelles : hyperviseur
	    \begin {itemize}
		\item exécuter les instructions virtuelles
		\item émuler les périphériques (et le réseau)
		\item exécuter les opérations privilégiées des invités
	    \end {itemize}
    \end {itemize}
\end {frame}


\begin {frame} {Classification}
    Tentative de classification :
    \begin {center}
	\fig {taxo} {.7}
    \end {center}
\end {frame}

\begin {frame} {Position de l'hyperviseur -- Type 0}
    \begin {minipage} {.60\linewidth}
	\begin {itemize}
	    \item les programmes virtuels s'exécutent directement
		sur la machine réelle
	    \item les ressources matérielles sont partitionnées
		entre les machines virtuelles
	    \item peu de flexibilité
	\end {itemize}
	Exemple : VM/370 (pas d'exemple actuel)
    \end {minipage} %
    \begin {minipage} {.39\linewidth}
	\fig {type0} {}
    \end {minipage}
\end {frame}

\begin {frame} {Position de l'hyperviseur -- Type 1}
    \begin {minipage} {.60\linewidth}
	\begin {itemize}
	    \item l'hyperviseur s'exécute directement sur
		le matériel
	    \item pas de système d'exploitation hôte
		\begin {itemize}
		    \item en réalité, l'hyperviseur est un
			noyau spécialisé
		    \item fonctions nécessaires pour se connecter
			à l'hyperviseur, gérer les machines virtuelles,
			etc.
		\end {itemize}
	    \item davantage de ressources pour les machines virtuelles

	\end {itemize}
	Exemples : VMware ESX, Xen, Hyper-V
    \end {minipage} %
    \begin {minipage} {.39\linewidth}
	\fig {type1} {}
    \end {minipage}
\end {frame}

\begin {frame} {Position de l'hyperviseur -- Type 1,5}
    \begin {minipage} {.60\linewidth}
	Type non \frquote{officiel}

	\bigskip

	\begin {itemize}
	    \item l'hyperviseur est intégré au système d'exploitation hôte
	    \item bonne intégration

	\end {itemize}

	Exemples : Linux KVM
    \end {minipage} %
    \begin {minipage} {.39\linewidth}
	\fig {type15} {}
    \end {minipage}
\end {frame}

\begin {frame} {Position de l'hyperviseur -- Type 2}
    \begin {minipage} {.60\linewidth}
	\begin {itemize}
	    \item l'hyperviseur utilise le système d'exploitation hôte
	    \item davantage de flexibilité

	\end {itemize}
	Exemples : VMware Workstation, VirtualBox
    \end {minipage} %
    \begin {minipage} {.39\linewidth}
	\fig {type2} {}
    \end {minipage}
\end {frame}

\begin {frame} {Type d'émulation -- Niveau machine}

    La virtualisation peut être réalisée au niveau du processeur :
    le système invité exécute les instructions du noyau comme s'il
    utilisait une machine réelle

    \begin {itemize}
	\item si le matériel supporte la virtualisation (cf plus loin) :

	    \begin {itemize}
		\item l'hyperviseur \alert{lance} le système
		    invité, puis il gère les \alert{opérations
		    privilégiées} qu'il demande

	    \end {itemize}

	\item si le matériel ne supporte pas la virtualisation :
	    \begin {itemize}
		\item l'hyperviseur \alert{modifie} le code du
		    noyau invité en préalable à son exécution

		    \begin {itemize}
			\item remplacement des instructions non
			    virtualisables par des appels à l'hyperviseur

		    \end {itemize}

	    \end {itemize}
    \end {itemize}

    \medskip

    Encore appelée : virtualisation complète (\emph{Full virtualization\/})
    \\
    \implique la plupart des hyperviseurs (VirtualBox, VMware, etc.)

\end {frame}

\begin {frame} {Type d'émulation -- Niveau noyau}
    La virtualisation est effectuée au niveau du noyau :

    \begin {itemize}
	\item \alert{conteneurs} (virtualisation légère)

	    \begin {itemize}

		\item isolation des machines par une couche
		    d'indirection pour virtualiser les objets du noyau

		\item \alert{pas de noyau invité} : le noyau
		    hôte supporte les applications
		    \begin {itemize}
			\item corollaire : la machine virtuelle a le
			    même noyau (type, version) que la machine
			    hôte

		    \end {itemize}

		\item exemples : LXC sur Linux, Jails sur FreeBSD

	    \end {itemize}

	\item \alert{émulation de système}
	    \begin {itemize}
		\item le noyau supporte les primitives systèmes d'un autre
		    système

		\item pas forcément de virtualisation des objets du noyau
		    \begin {itemize}
			\item but = exécuter des applications
			    du système X sur un système Y

		    \end {itemize}

		\item exemples : émulation Linux sur FreeBSD, Linux
		    shell sur MS-Windows 10, Wine sur Unix

	    \end {itemize}

    \end {itemize}

    \implique on ne peut pas vraiment parler de virtualisation
\end {frame}

\begin {frame} {Type d'émulation -- Niveau noyau -- Conteneurs}
    Principe : \frquote{enfermer} des processus dans un
    environnement composé d'objets du noyau virtualisés
    \begin {itemize}
	\item constituer des sous-ensembles des objets
	    \begin {itemize}
		\item \code{chroot} : donner la vision
		    d'une partie du système de fichiers
		\item regrouper les processus d'un même système
		    virtuel dans la table des processus
	    \end {itemize}
    \end {itemize}
    \begin {minipage} {.65\linewidth}
	\begin {itemize}
	    \item grand succès
	    \item pas besoin de support matériel
	    \item davantage de flexibilité : environnement plus simple
	    \item utilitaires pour gérer des systèmes
		virtuels  (Ex : Docker)

	\end {itemize}
    \end {minipage} %
    \begin {minipage} {.34\linewidth}
	\fig {cont} {}
    \end {minipage}

    \bigskip 
    Exemples : LXC sur Linux, Jails sur FreeBSD
\end {frame}

\begin {frame} {Type d'émulation -- Émulation logicielle}
    Programme qui émule le comportement d'un processeur et de sa
    périphérie

    \begin {itemize}
	\item Permet de simuler n'importe quel processeur sur n'importe
	    quel processeur réel

	\item Portable
	\item Lent : ne respecte pas le \alert{critère de performances}
	\item Exemples : programmes \code{simh}, \code{qemu} (si pas
	    d'utilisation de KVM)

    \end {itemize}

    \implique on ne peut pas vraiment parler de virtualisation
\end {frame}

\begin {frame} {Modification du système virtuel}
    Dans certains cas, la virtualisation passe par une modification du
    système virtuel

    \bigskip

    \begin {itemize}
	\item système identique : pas de modification (vraie virtualisation)

	\item système adapté (\alert{\emph{paravirtualisation\/}})
	    \begin {itemize}
		\item le noyau est modifié explicitement pour
		    l'environnement virtuel

		\item utilisé pour :
		    \begin {itemize}
			\item simplifier le noyau virtuel :
			    éliminer les périphériques
			    \frquote{baroques}

			\item tenir compte de l'absence de support
			    matériel \\
			    opérations privilégiées
			    $\rightarrow$ appels à l'hyperviseur

		    \end {itemize}

		\item modification du noyau
		    \implique ne respecte pas le \alert{critère de
		    fidélité}

		\item exemple : hyperviseur Xen
		    \begin {itemize}
			\item les principaux noyaux (FreeBSD, Linux,
			    Windows) sont \frquote{xenifiés}

		    \end {itemize}

	    \end {itemize}

	\item pas de noyau : cf \alert{conteneurs}

    \end {itemize}
\end {frame}

\begin {frame} {Classification}
    Situer les principaux systèmes de virtualisation :

    \ctableau {\fC} {|l|c|c|c|} {
	\textbf{Système}
	    & \multicolumn {1} {p{.25\linewidth}|}
		{\centering\textbf{Position de l'hyperviseur}}
	    & \multicolumn {1} {p{.25\linewidth}|}
		{\centering\textbf{Modification du système virtuel}}
	    & \multicolumn {1} {p{.25\linewidth}|}
		{\centering\textbf{Type d'émulation}}
	    \\ \hline
	    Xen
		& type 1
		& adapté
		& virtu. complète
		\\
	    Hyper-V
		& type 1
		& identique
		& virtu. complète
		\\
	    KVM
		& type 1,5
		& identique
		& virtu. complète
		\\
	    VirtualBox
		& type 2
		& identique
		& virtu. complète
		\\
	    LXC
		& type 1,5
		& pas de noyau
		& conteneur
		\\
	    Bash / W10
		& type 1,5
		& pas de noyau
		& niveau noyau
		\\
    }

\end {frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Virtualisation complète
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Virtualisation complète}

\begin {frame} {Virtualisation complète}
    Fonctionnement d'un hyperviseur pour la virtualisation complète :

    \bigskip

    \begin {minipage} {.65\linewidth}
	\begin {itemize}
	    \item machine virtuelle = un \alert{unique} processus
		du système hôte

	    \begin {itemize}
		\item quels que soient les processus du système invité
		    \begin {itemize}
			\item pas de correspondance entre processus des
			    SE invité et hôte
		    \end {itemize}
		\item exécution en mode \alert{non privilégié}
	    \end {itemize}
	\end {itemize}
    \end {minipage} %
    \begin {minipage} {.34\linewidth}
	\fig {kern-vm} {}
    \end {minipage}

    \bigskip

    Machine virtuelle dans un processus utilisateur du système hôte :
    \begin {itemize}
	\item comment le noyau du SE invité peut exécuter des
	    instructions privilégiées ?
	\item comment les processus invités peuvent faire 
	    des appels au noyau invité ?

    \end {itemize}
\end {frame}

\begin {frame} {Virtualisation complète -- Trap \& Emulate}
    \begin {itemize}
	\item lorsque le noyau invité exécute une instruction privilégiée

	    \begin {itemize}
		\item l'hyperviseur détecte une \alert {tentative
		    d'exécution d'une instruction privilégiée} par un
		    processus en mode utilisateur
		\item exception \implique entrée dans le noyau hôte
		    (= hyperviseur)
		\item l'hyperviseur peut alors \alert{simuler} l'action
		    demandée
		    \begin {itemize}
			\item simuler une entrée/sortie avec un périphérique
			\item simuler une modification de l'état du processeur
			    \\
			    \implique ex : registre d'état SR \frquote{virtuel}
		    \end {itemize}
	    \end {itemize}

	\item lorsqu'un processus invité exécute l'instruction
	    \alert{trap} (rappel : primitive système)

	    \begin {itemize}
		\item exception \implique entrée dans le noyau hôte
		\item l'hyperviseur simule les actions effectuées par
		    le processeur
		    \begin {itemize}
			\item cf chapitre \frquote{mécanismes de base}
		    \end {itemize}
	    \end {itemize}
    \end {itemize}

    Fonctionnement de type \frquote{\alert{trap-and-emulate}}
\end {frame}

\begin {frame} {Virtualisation complète -- Trap \& Emulate}
    \begin{center}
	\fig {trap-emul} {.9}
    \end{center}
\end {frame}

\begin {frame} {Virtualisation complète}
    Tirer parti des caractéristiques du matériel :

    \begin {itemize}
	\item rappel : deux modes d'exécution (privilégié et non
	    privilégié) nécessaires a minima pour répondre aux
	    deux objectifs d'un système d'exploitation
    \end {itemize}

    \smallskip

    \begin {minipage} {.70\linewidth}
    \begin {itemize}
	\item exemple : l'architecture x86 dispose de 4 niveaux
	    (\frquote{anneaux})
	    \begin {itemize}
		\item anneau 0 : mode le plus privilégié
		\item anneau 3 : mode le moins privilégié
		\item anneaux 1 et 2 intermédiaires : souvent non utilisés
	    \end {itemize}
    \end {itemize}
    \end {minipage}
    \begin {minipage} {.29\linewidth}
	\fig {ring} {}
    \end {minipage}

    \smallskip

    \begin {itemize}
	\item utilisation d'un des deux niveaux intermédiaires pour
	    le noyau du système invité
	    \\
	    \implique \alert{meilleure isolation} entre les processus et
		le noyau du système invité

    \end {itemize}
\end {frame}

\begin {frame} {Virtualisation complète -- Support matériel}
    Le schéma \frquote{trap-and-emulate} nécessite que l'hyperviseur
    détecte \textbf{toutes} les instructions privilégiées

    \medskip

    Contre-exemple : l'architecture x86 \alert{avant} 2006
    \begin {itemize}
	\item certaines instructions du processeur ont un comportement
	    différent en mode privilégié et non privilégié
	    \begin {itemize}
		\item exemple : instruction \code{popf} =
		    restauration du registre d'état à partir de la pile

		\item instruction normalement privilégiée
		    \begin {itemize}
			\item en mode non privilégié, cette instruction
			    est \alert{valide} et restaure seulement
			    \alert{certains flags}

			\item le processeur ne génère pas d'exception

		    \end {itemize}

	    \end {itemize}

	\item l'hyperviseur ne peut donc pas savoir qu'une instruction
	    privilégiée a tenté de modifier le registre d'état
	    \begin {itemize}
		\item l'hyperviseur ne peut pas mettre à jour le registre
		    d'état virtuel
	    \end {itemize}
    \end {itemize}

    \medskip

    \implique support matériel insuffisant
\end {frame}

\begin {frame} {Virtualisation complète -- Support matériel}
    En l'absence de support matériel, l'hyperviseur doit :

    \begin {itemize}
	\item analyser le code exécutable du noyau au chargement
	\item remplacer \frquote{au vol} les instructions déficientes
	    par des appels à l'hyperviseur

	\item fonctionnement de type \frquote{\alert{binary translation}}

    \end {itemize}

    \medskip

    Avant 2006 :

    \begin {itemize}
	\item premiers hyperviseurs sur x86 = binary translation
	\item autre approche = paravirtualisation (exemple : Xen)
    \end {itemize}

    \medskip

    À partir de 2006 :
    \begin {itemize}
	\item support matériel ajouté (Intel VT-x et AMD-V)
    \end {itemize}
\end {frame}

\begin {frame} {Virtualisation complète -- Support matériel}
    Exemple : Intel VT-x (VMX = \emph{Virtual Machine eXtensions\/})

    \begin {itemize}
	\item mode spécifique \frquote{machine virtuelle} (VMX) dans le
	    processeur

	    \begin {itemize}
		\item deux sous-modes : VMX \alert{non-root} et VMX
		    \alert{root}

		    \smallskip

		    \ctableau {\fE} {|l|l|} {
			\tcol{Mode}{|} & \tcol{Utilisation}{} \\ \hline
			VMX non-root & noyau et processus invités \\
			VMX root & hyperviseur \\
			\hline
			hors VMX & noyau hôte \\
		    }

		    \medskip

		\item en mode VMX non-root, le processeur gère un
		    \frquote{état virtuel}

		    \begin {itemize}
			\item sauvegarde (resp. restauration) des
			    registres à la sortie (resp. à l'entrée)
			    du mode VMX non-root + déroutement
			    \\
			    \implique analogue au traitement classique
			    d'interruption / exception

			\item opérations restreintes, même
			    dans l'anneau 0 (mode privilégié)
			    \\
			    \implique mode \frquote{prison} pour machines
			    virtuelles

		    \end {itemize}
	    \end {itemize}

	\item quelques nouvelles instructions :
	    \begin {itemize}
		\item \code{vmxon} : activer
		    le mode VMX et passer en mode VMX root

		\item \code{vmresume} : rentre en mode VMX non-root
		    à partir de l'état d'une machine virtuelle
		    sauvegardé lors d'une précédente sortie du mode
		    VMX non-root

	    \end {itemize}

    \end {itemize}
\end {frame}

\begin {frame} {Virtualisation complète -- Support matériel}
    \begin {center}
	\fig {vmx} {.6}
    \end {center}
    \begin {itemize}
	\item VMX non-root
	    \begin {itemize}
		\item interruption \implique sortie de la machine virtuelle
		\item le processeur peut passer dans l'anneau 0 (noyau virtuel)
	    \end {itemize}
	\item VMX root
	    \begin {itemize}
		\item l'hyperviseur peut simuler une interruption avec
		    \code{vmresume}

	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Virtualisation complète -- Gestion mémoire}
    \begin {center}
	\fig {vmem} {.7}
    \end {center}

    \begin {itemize}
	\item À quelle adresse physique correspond la page numéro 1
	    du processus $P_1$ de la machine virtuelle ?
	    \begin {itemize}
		\item c'est normalement le noyau invité qui initialise
		    la table des pages
		\item le noyau invité ne connaît pas les adresses
		    physiques des pages de son propre espace d'adressage
		\item le noyau hôte peut déplacer une page de la
		    machine virtuelle

	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Virtualisation complète -- Gestion mémoire}
    Solution :
    \begin {itemize}
	\item le noyau invité initialise la table des pages avec
	    \alert{ses} adresses (dans son propre espace d'adressage)
	\item le noyau invité modifie le registre d'adresse de
	    la table des pages
	\item instruction privilégiée \implique notification de l'hyperviseur
	\item l'hyperviseur modifie la table avec les adresses physiques
	    réelles
	    \begin {itemize}
		\item l'hyperviseur doit conserver une
		    \frquote{\emph{shadow page table}}
	    \end {itemize}

    \end {itemize}

    \bigskip

    Mais problème :
    \begin {itemize}
	\item le noyau invité peut modifier la table des
	    pages en mémoire
	\item l'hyperviseur ne pourra pas détecter la modification
    \end {itemize}

\end {frame}

\begin {frame} {Virtualisation complète -- Support matériel}
    Support matériel pour la traduction d'adresses :
    \begin {itemize}
	\item AMD : \emph{Nested Page Tables}
	\item Intel : \emph{Extended Page Tables} (EPT)
    \end {itemize}

    \bigskip

    Idée :
    \begin {itemize}
	\item laisser le noyau invité gérer la table de pages pour
	    traduire les adresses logiques du processus invité en
	    adresses physiques dans le noyau invité :
	    \alert{$\varphi_i = f(\lambda_i)$}

	\item extension MMU : à chaque fois que la MMU obtient une
	    adresse physique dans le noyau invité, traduction en adresse
	    physique dans la mémoire physique de l'hôte :
	    \alert{$\varphi_h = g(\varphi_i)$}

    \end {itemize}
\end {frame}

\begin {frame} {Virtualisation complète -- Support matériel}
    Implémentation Intel EPT :

    \begin {enumerate}
	\item le processus invité utilise une adresse logique (invité)
	    $\lambda_i$

	\item la MMU consulte le registre CR3 pour avoir l'adresse
	    physique (invité) de la table des pages
	    \begin {itemize}
		\item la MMU traduit cette adresse par le mécanisme EPT
		    pour obtenir l'adresse physique (hôte) de la table
		    des pages
	    \end {itemize}
	\item la MMU consulte la table des pages à l'adresse (hôte indiquée)
	    et obtient l'adresse physique (invité) de la table secondaire
	    \begin {itemize}
		\item la MMU traduit cette adresse par le mécanisme EPT
		    pour obtenir l'adresse physique (hôte) de la table
		    secondaire
	    \end {itemize}
	\item la MMU consulte la table des pages secondaire à l'adresse
	    (hôte indiquée) et obtient l'adresse physique (invité)
	    de la page
	    \begin {itemize}
		\item la MMU traduit cette adresse par le mécanisme EPT
		    pour obtenir l'adresse physique (hôte) de la page
	    \end {itemize}
	\item la MMU forme ensuite l'adresse physique (hôte) $\varphi_h$

    \end {enumerate}
\end {frame}

\begin {frame} {Virtualisation complète -- Support matériel}
    Le mécanisme EPT pour traduire une adresse physique $\varphi_i$
    (invité) en adresse physique $\varphi_h$ (hôte) consiste en :
    \begin {itemize}
	\item traduction classique d'adresse
	    \begin {itemize}
		\item cf chapitre sur la gestion mémoire
	    \end {itemize}

	\item la traduction est effectuée avec 64 bits
	    \begin {itemize}
		\item les extensions Intel VT-x ne sont utilisables
		    qu'avec un hôte 64 bits

	    \end {itemize}

	\item pointeur EPTP (\emph{EPT Pointer\/})
	    \begin {itemize}
		\item pointeur sur la table de traduction d'adresse EPT
		\item propre à chaque machine virtuelle
		\item localisé dans la structure de contrôle VMX
	    \end {itemize}

    \end {itemize}

    \bigskip

    Nombreux accès à la mémoire pour la traduction d'adresse :
    \begin {itemize}
	\item importance du TLB (cache des traductions)
	\item utilisation de \frquote{grandes} pages si la MMU les
	    supporte
	    \begin {itemize}
		\item minimisation de la \frquote{pollution} du TLB
	    \end {itemize}
    \end {itemize}

\end {frame}

\begin {frame} {Virtualisation complète -- Support matériel}
    \begin {center}
	\fig {ept} {1}
    \end {center}
\end {frame}

\begin {frame} {Virtualisation complète -- Support matériel}
    Virtualisation des entrées/sorties :

    \begin {itemize}
	\item support matériel également nécessaire pour la
	    virtualisation des entrées/sorties
	    \begin {itemize}
		\item tous les accès aux périphériques ne peuvent
		    pas être simulés par l'hyperviseur
		    \begin {itemize}
			\item exemple : support d'un périphérique
			    exotique (non reconnu par le noyau hôte)
			    dans un noyau invité

			\item exemple : accès à la carte graphique

		    \end {itemize}
	    \end {itemize}

	\item virtualisation également des interruptions

    \end {itemize}
\end {frame}
