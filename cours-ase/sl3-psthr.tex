\incdir {inc3-psthr}

\titreA {Processus et threads}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Constituants d'un processus
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Constituants d'un processus}

\begin {frame} {Définition d'un processus}
    Définition (haut niveau) :

    \begin {quote}
	un processus est une instance d'un programme en cours d'exécution
    \end {quote}

    ... mais pas n'importe quelle exécution :

    \begin {itemize}
	\item j'exécute \code{ls /tmp} : le processus correspond à
	    l'exécution du programme \code{ls} avec les données
	    \code{/tmp}

	\item quelqu'un d'autre exécute \code{ls /tmp} en même
	    temps : ce n'est pas la même exécution, même si c'est le
	    même programme et les mêmes données

	    \begin {itemize}
		\item ce n'est pas le même processus
		\item même si le \frquote{quelqu'un d'autre}, c'est moi
	    \end {itemize}

    \end {itemize}
\end {frame}

\begin {frame} {Définition d'un processus}
    Différence entre programme et processus :

    \begin {itemize}
	\item \textbf{programme} = objet \alert{inanimé}
	    \begin {itemize}
		\item code exécutable (séquence d'instructions encodées)
		    \begin {itemize}
			\item plus des données (ex: \code{int x = 5 ;} en C)
		    \end {itemize}
		\item matérialisé par un fichier exécutable
	    \end {itemize}
	\item \textbf{processus} = ce qui permet d'\alert{animer} un programme
	    \begin {itemize}
		\item contexte nécessaire pour exécuter le programme
		    \begin {itemize}
			\item mémoire centrale
			    \implique localisation du programme
			\item temps partagé, interruptions
			    \implique sauvegarde des registres
			\item attente de ressource non disponible
			    \implique état, pile noyau
			\item etc.
		    \end {itemize}

		\item descripteur de processus
			(\emph{process control block\/})
		    \begin {itemize}
			\item contient toutes ces données nécessaires
			    à l'exécution
			\item et beaucoup d'autres...
		    \end {itemize}
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Descripteur de processus}
    \begin {itemize}
	\item rappel du chapitre précédent : le descripteur contient
	     \begin {itemize}
		\item sauvegarde des registres utilisateurs
		\item sommet de la pile noyau
		\item état (prêt à tourner, en attente)
		\item copie des registres LIM et BASE pour la MMU
	     \end {itemize}

	\item d'autres informations sont utiles

	    \begin {itemize}
		\item identité du processus

		    \begin {itemize}
			\item pour désigner un processus
			    (par exemple : \textit{process-id\/})
		    \end {itemize}

		\item quel processus choisir parmi les processus prêts
		    à tourner ?

		    \begin {itemize}
			\item paramètres possibles : priorité définie, temps
			    d'attente, temps d'utilisation du CPU, etc.
		    \end {itemize}

		\item informations nécessaires pour l'API POSIX

		    \begin {itemize}
			\item identité du processus parent,
			    propriétaire, table des ouvertures de
			    fichiers, actions associées aux signaux,
			    répertoire courant, etc.

		    \end {itemize}

	    \end {itemize}

    \end {itemize}

    \implique informations placées dans le descripteur de processus
    \\
    { \fB (cf \code{task\_struct} sur
	\url {http://www.tldp.org/LDP/tlk/ds/ds.html} )}
\end {frame}

\begin {frame} {États d'un processus}
    L'état d'un processus est placé dans son descripteur :

    \begin {center}
	\fig {etats} {.7}
    \end {center}

    Initialement :
    \begin {itemize}
	\item le processus est créé dans l'état \frquote{prêt à tourner}

	\item il y reste jusqu'à ce qu'il soit élu pour être mis
	    sur le processeur

	\item il reste sur le processeur jusqu'à :
	    \begin {itemize}
		\item la fin du temps qui lui est alloué (quantum), ou
		\item ce qu'il demande une ressource non disponible, ou
		\item ce qu'il demande à se terminer
	    \end {itemize}

	\item etc.

    \end {itemize}
\end {frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Mémoire d'un processus
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Mémoire d'un processus}

\begin {frame} {Espace mémoire d'un processus}
    L'espace mémoire d'un processus est découpé en 3 zones :

    \medskip

    \begin {minipage} [c] {.29\linewidth}
	\fig {psmem-1} {}
    \end {minipage}
    \hfill
    \begin {minipage} [c] {.69\linewidth}
	\begin {itemize}
	    \fB
	    \item segment \frquote{text}
		\begin {itemize}
		    \fC
		    \item programme (code compilé)
		    \item adresse 0 pas utilisée : pourquoi ?
		\end {itemize}
	    \item segment \frquote{data}
		\begin {itemize}
		    \fC
		    \item variables globales (+ \code{static} locales)
		    \item tas (mémoire allouée par \code{malloc})
		    \item extension explicite (via \code{malloc})
		\end {itemize}
	    \item segment \frquote{stack} : la pile d'exécution
		\begin {itemize}
		    \fC
		    \item variables locales
		    \item arguments des fonctions
		    \item adresses de retour
		    \item extension implicite (utilisation de la pile)
		\end {itemize}
	    \item d'autres zones peuvent être ajoutées
		\begin {itemize}
		    \fC
		    \item bibliothèques partagées
		    \item mémoire partagée entre processus
		    \item \implique cf chapitre sur la mémoire
		\end {itemize}
	\end {itemize}
    \end {minipage}

\end {frame}


\begin {frame} {Espace mémoire d'un processus}
    \begin {center}
	\fig {var} {}
    \end {center}
\end {frame}


\begin {frame} {Espace mémoire d'un processus}
    Anatomie d'un fichier exécutable :

    \bigskip

    \begin {minipage} [c] {.29\linewidth}
	\fig {psmem-2} {}
    \end {minipage}
    \hfill
    \begin {minipage} [c] {.69\linewidth}
	\begin {itemize}
	    \fC
	    \item en-tête
		\begin {itemize}
		    \fD
		    \item nombre magique
		    \item description des différentes parties
		    \item taille du bss (= données non initialisées)
		\end {itemize}
	    \item table des symboles :
		\begin {itemize}
		    \fD
		    \item adresse de chaque symbole global
		    \item noms et emplacements de l'utilisation des symboles
			\frquote{non résolus}
		\end {itemize}
	    \item code binaire : code compilé
	    \item données initialisées : initialisation des variables globales
		\begin {itemize}
		    \fD
		    \item ex : \code{int b = 6 ;}
		\end {itemize}
	    \item informations de debug :
		\begin {itemize}
		    \fD
		    \item  associations <fichier, numéro de ligne, adresse dans le code>
		\end {itemize}
	\end {itemize}
    \end {minipage}
\end {frame}


\begin {frame} {Espace mémoire d'un processus}
    Sur Linux, les fichiers exécutables (et les \frquote{\code{.o}}) sont
    au format ELF \\
    (\textit{Executable and Linkable Format\/})
    \begin {itemize}
	\item 4 premiers octets : \code{0x7f}, \code{0x45}, \code{0x4c},
	    \code{0x46} (= \code{7f} + '\code{ELF}')
	\item format générique, plusieurs sections dans le fichier
    \end {itemize}

    \medskip

    Commandes utiles :
    \begin {itemize}
	\item \code{size} : affiche la taille des segments text et data, ainsi que le bss
	\item \code{nm} : affiche la table des symboles d'un exécutable
	\item \code{strip} : supprime la table des symboles d'un exécutable
	\item \code{readelf} : affiche des informations sur le fichier
	    et ses sections
	\item \code{objdump} : affiche des parties du fichier :
	    \begin {itemize}
		\item \code{objdump -d} : \emph{désassemble} le code exécutable
		\item \code{objdump -g} : affiche les informations de debug
		\item etc.
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Espace mémoire d'un processus}
    Initialisation de l'espace mémoire à partir du fichier
    exécutable :

    \begin {center}
	\fig {psmem-3} {.8}
    \end {center}

    Note : le segment \frquote{data} est initialisé à partir du fichier,
    le reste du segment (i.e. la taille du bss) est initialisé à 0.

\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ordonnancement
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Ordonnancement}

\begin {frame} {Commutation et ordonnancement}
    Deux problèmes distincts :
    \begin {enumerate}
	\item \textbf{commutation}
	    \begin {itemize}
		\item comment réaliser la commutation entre deux processus ?
		\item voir chapitre précédent
	    \end {itemize}
	\item \textbf{ordonnancement}
	    \begin {itemize}
		\item comment choisir le processus à faire tourner ?
		\item définir une stratégie
	    \end {itemize}
    \end {enumerate}
\end {frame}

\begin {frame} {Ordonnancement -- Stratégie}
    Choisir le meilleur processus suivant quel critère ?
    \begin {center}
	\fig {crit} {.7}
    \end {center}

    Critères parfois contradictoires \implique compromis
\end {frame}

\begin {frame} {Ordonnancement -- Critères}
    \begin {itemize}
	\item maximiser l'utilisation des ressources physiques :
	    \begin {itemize}
		\item processeur, périphériques \\
		\item un bon système est un système utilisé, pas un
		    système oisif
	    \end {itemize}
	\item augmenter le débit 
	    \begin {itemize}
		\item nombre de processus terminés par unité de temps
	    \end {itemize}
	\item augmenter l'affinité
	    \begin {itemize}
		\item conserver le même processeur
		    pour bénéficier du cache
	    \end {itemize}
	\item diminuer le temps d'attente
	    \begin {itemize}
		\item remettre le processus sur le processeur dès la fin
		    d'une E/S
	    \end {itemize}
	\item minimiser la durée jusqu'à la terminaison
	    \begin {itemize}
		\item conserver le processeur le plus longtemps possible
	    \end {itemize}
	\item améliorer le temps de réponse interactif
	    \begin {itemize}
		\item privilégier les processus attendant le plus
	    \end {itemize}
	\item diminuer la latence
	    \begin {itemize}
		\item ... entre un événement (exemple : signal) et
		    la remise du processus sur le processeur
		\item prérequis pour les systèmes temps-réel
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Ordonnancement -- Algorithmes}
    Algorithmes classiques d'ordonnancement :

    \begin {itemize}
	\fB
	\item FCFS (First Come, First Served) : premier arrivé, premier servi

	    \begin {itemize}
		\fC
		\item peu compatible avec un système préemptif
	    \end {itemize}

	\item SJF (Shortest Job First) : le plus court en premier

	    \begin {itemize}
		\fC
		\item algorithme optimal...
		    \begin {itemize}
			\fD
			\item minimise le temps d'attente de l'ensemble
			    des processus
		    \end {itemize}
		\item ... mais impraticable sans connaître le futur !
	    \end {itemize}

	\item RR (Round Robin) : tourniquet

	    \begin {itemize}
		\fC
		\item le plus simple
	    \end {itemize}

	\item RT (Real Time) : temps réel

	    \begin {itemize}
		\fC
		\item distingue deux (ou plus) catégories de processus :
		    \begin {itemize}
			\fD
			\item les processus \frquote{temps réel} : à ordonnancer
			    en priorité
			\item les processus \frquote{normaux} : passent après
			    les processus temps réel
		    \end {itemize}
	    \end {itemize}

    \end {itemize}
\end {frame}

\begin {frame} {Ordonnancement -- Exemple}
    Algorithme d'Unix (initial) = approximation de SJF

    \begin {itemize}
	\item chaque processus a un compteur \frquote{\alert{utilisation CPU}}
	\item à chaque interruption d'horloge, le compteur
	    du processus \textbf {courant} est incrémenté
	\item chaque seconde, le compteur de \textbf {tous} les processus
	    est divisé par 2
	    \begin {itemize}
		\item \emph{decay function} (fonction de décroissance)
	    \end {itemize}
	\item priorité = compteur + valeur de \code{nice}
	    \begin {itemize}
		\item \code{int nice (int increment)}
	    \end {itemize}
	\item choix du processus : priorité minimum
	\item décision : à chaque retour en mode \frquote{utilisateur}
	    \begin {itemize}
		\item à chaque retour d'interruption ou d'exception
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Ordonnancement -- Exemple}
    \begin {center}
	\fig {decay} {}
    \end {center}

    \begin {itemize}
	\item plus un processus consomme, moins il est prioritaire
	\item division par 2 \implique historique de consommation
	    disparaît peu à peu

	\item privilégie les processus interactifs
	    \begin {itemize}
		\item c'est-à-dire ceux qui ont récemment consommé
		    le moins de CPU
	    \end {itemize}
    \end {itemize}
    \implique pas de notion de \emph{quantum de temps}
\end {frame}

\begin {frame} {Ordonnancement -- Exemple}
    Algorithme d'Unix (initial) = approximation de SJF

    \begin {itemize}
	\item consommation future de CPU = prédiction basée sur
	    l'historique de consommation
	    \begin {itemize}
		\item historique récent
		\item atténuation rapide (fonction de décroissance
		    exponentielle)

	    \end {itemize}
	\item consommation future $h_{n+1}$ estimée toutes les secondes
	    à partir :
	    \begin {itemize}
		\item de la consommation actuelle $c_n$ (augmentation
		    du compteur)
		\item de l'historique récent $h_n$ (l'ancienne valeur
		    du compteur)
		    \begin {itemize}
			\item initialement, $h_0 = 0$
		    \end {itemize}
	    \end {itemize}
	\item prédiction : $h_{n+1} = \alpha c_n + (1-\alpha) h_n$,
	    avec $\alpha = \frac{1}{2}$
	    \begin {itemize}
		\item si $\alpha = 1$, pas de prise en compte de l'historique
		\item si $\alpha = 0$, pas d'intérêt
		\item si $\alpha = \frac{1}{2}$,
		    l'historique décroît exponentiellement
		    \begin {itemize}
			\item $h_{n+1} = \frac {1} {2} c_n
					+ \frac {1} {4} c_{n-1}
					+ \frac {1} {8} c_{n-2}
					\ldots
					+ \frac {1} {2^{n+1}} c_0$
		    \end {itemize}
	    \end {itemize}
	\item sélection du processus tel que $h_{n+1}$ soit le minimum

    \end {itemize}
\end {frame}

\begin {frame} {Ordonnancement -- Exemple}
    \lst {it-horl.c} {\fE} {}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Les threads
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Introduction aux threads}

\begin {frame} {Limitations des processus}

    Les processus souffrent de plusieurs limitations :

    \begin {itemize}
	\item pas d'utilisation du parallélisme matériel \\
	    \implique si un ordinateur a plusieurs processeurs (ou
		plusieurs cœurs), un processus ne peut en utiliser
		qu'un seul


	\item commutation de processus lente \\
	    \implique sauvegarde et restauration du contexte,
		sélection du processus, \emph {changement d'espace
		d'adressage}
		\\
	    \implique cache des traductions d'adresses

	\item étanchéité des espaces mémoires \\
	    \implique mécanismes de communication inter-processus
	    (tubes, sockets) lents

    \end {itemize}
\end {frame}

\begin {frame} {Limitations des processus}
    Un processus traditionnel rassemble \alert{deux notions} distinctes :

    \begin {itemize}
	\item \alert{accès à des ressources}
	    \begin {itemize}
		\item espace mémoire
		\item ouvertures de fichiers
		\item etc.
	    \end {itemize}
	\item \alert{exécution de programme}
	    \begin {itemize}
		\item registres du processeur
		\item pile d'exécution
	    \end {itemize}
    \end {itemize}

    \bigskip

    Pourquoi ne pas dissocier ces deux notions ? \\
    \implique apparition des \alert{threads} dans les années 1980

\end {frame}

\begin {frame} {Threads}
    % Début des années 1980~: apparition des \emph {threads} (ou \emph
    % {processus légers})

    \begin {itemize}
	\item plusieurs entités d'exécution (\emph{threads\/})...
	\item ... dans un même espace mémoire (\emph{processus\/})
    \end {itemize}

    \medskip

    \begin {center}
	\fig {thr-intro} {.6}
    \end {center}

    \medskip

    Dorénavant, chaque processus a par défaut un \alert{thread
    principal}
    \\
    \implique c'est lui qui exécute la fonction \code{main} (en C)

\end {frame}

\begin {frame} {Threads}
    Intérêts des threads (par rapport aux processus) :
    \begin {itemize}
	\item exploiter le parallélisme matériel
	    \begin {itemize}
		\item chaque thread est une entité d'exécution et peut
		    se voir attribuer un processeur (ou un cœur)
	    \end {itemize}
	\item alléger la commutation
	    \begin {itemize}
		\item pour commuter entre deux threads d'un processus,
		    il n'y a pas besoin de changer les registres de la MMU
	    \end {itemize}
	\item alléger la création et la terminaison des threads
	    \begin {itemize}
		\item beaucoup plus rapide et efficace que les processus
	    \end {itemize}
	\item accélérer la communication entre entités d'exécution
	    \begin {itemize}
		\item espace mémoire du processus partagé par
		    tous les threads du processus \implique communication
		    rapide

	    \end {itemize}
	\item faciliter la rédaction de programmes comportant des
	    activités autonomes
	    \begin {itemize}
		\item exemples : serveur Web traitant beaucoup de clients
		    simultanément, jeu gérant plusieurs sources
		    d'événements

	    \end {itemize}

    \end {itemize}

\end {frame}

\begin {frame} {Threads}
    Inconvénients des threads (par rapport aux processus) :
    \begin {itemize}
	\item pas d'isolation mémoire des threads entre eux
	    \begin {itemize}
		\item pas de protection contre ses propres erreurs
		\item \frquote{un bon vieux Segfault vaut mieux qu'une
		    corruption silencieuse de la mémoire}
	    \end {itemize}

	\item communication directe via la mémoire
	    \begin {itemize}
		\item puis-je utiliser la donnée que le thread voisin
		    est censé avoir produite ?
		\item nécessite des outils de synchronisation (cf
		    chapitre suivant)

	    \end {itemize}

	\item gestion d'activités asynchrones
	    \begin {itemize}
		\item existe déjà avec les processus, mais difficulté
		    amplifiée par le partage de la mémoire
	    \end {itemize}
    \end {itemize}

    \medskip

    \implique développement et mise au point un peu plus délicats

\end {frame}

\begin {frame} {Threads}
    Les threads d'un processus partagent~:
    \begin {itemize}
	\item l'identificateur du processus et son propriétaire
	\item l'\emph {espace mémoire unique} du processus
	\item les fichiers ouverts
	\item les actions associées aux signaux
	\item etc.
    \end {itemize}

    \medskip

    Chaque thread dispose~:
    \begin {itemize}
	\item de son identificateur de thread
	\item de sa propre entité d'exécution \\
	    \implique sauvegarde et restauration des registres
	\item de sa propre pile d'exécution
    \end {itemize}

\end {frame}

\begin {frame} {Threads}
    Espace d'adressage du processus~:
    \begin {minipage} {.65\linewidth}
	\begin {itemize}
	     \item zone \frquote{data} : variables globales

		\implique partagées par les threads

	     \item zone \frquote{stack} : chaque thread a sa
		propre pile d'exécution

		Attention : taille de la pile bornée \\
		\implique pas d'accroissement possible \\
		\implique taille fixée à la création \\
		\implique pas de détection de saturation !
	\end {itemize}
    \end {minipage}
    \hfill
    \begin {minipage} {.33\linewidth}
	\fig {thr-stack} {}
    \end {minipage}

\end {frame}

\begin {frame} {Threads -- Implémentation}
    Deux types extrêmes d'\textbf{implémentation} des threads
    \begin {itemize}
	\item \textit{User-level threads}
	    \begin {itemize}
		\item bibliothèque de fonctions pour gérer les threads
		\item pas d'appel au noyau pour la commutation entre threads
		    \begin {itemize}
			\item commutation par la bibliothèque
			    \implique très rapide
			\item le noyau n'ordonnance que le processus
			    \implique pas de parallélisme
			\item implémentations souvent partielles
		    \end {itemize}
		\item des appels au noyau peuvent bloquer
		    \begin {itemize}
			\item exemple : \code{read} sur un terminal
			\item utilisation des E/S asynchrones :
			    \code{open (... | O\_ASYNC ...)}
			\item il faut définir une \frquote{surcouche} au dessus
			    de \code{open} et \code{read}

		    \end {itemize}
		\item seule implémentation possible sur des systèmes anciens
	    \end {itemize}
	\item \textit{Kernel-level threads}
	    \begin {itemize}
		\item chaque thread est connu du noyau
		    \begin {itemize}
			\item si le noyau bloque un thread, les autres
			    peuvent continuer

		    \end {itemize}
		\item commutation par le noyau
		    \begin {itemize}
			\item commutation plus lente qu'en mode utilisateur
			\item entité d'ordonnancement = thread
			    \implique parallélisme possible
		    \end {itemize}
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Threads -- Implémentation}
    Qui connaît quelle information ?
    \begin {center}
	\fig {thr-ultklt} {.6}
    \end {center}
\end {frame}

\begin {frame} {Threads -- Implémentation}
    Performances comparées (en $\mu$s, sur un VAX mono-processeur) :

    \vspace* {-4mm}		% l'espace naturel est trop important

    \ctableau {\fD} {|p{.3\textwidth}|c|c|c|} {
	\textbf{Opération} & \textbf{User-level threads} & \textbf{Kernel-level threads} & \textbf{Processus} \\
	\hline
	Null fork : création et terminaison d'un processus/thread & 34 & 948 & \numprint{11300} \\
	Signal wait : envoi d'un signal et attente d'un acquittement & 37 & 441 & \numprint{1840} \\
    }

    \credit {\centerline{Extrait de \frquote{Operating Systems Internals and Design Principles}, W. Stallings}}

    \bigskip

    Bilan :
    \begin {itemize}
	\item les threads sont plus légers que les processus
	\item les threads \emph{user-level} sont plus légers
	    que les \emph{kernel-level}

	\item les résultats dépendent de chaque application
	    \begin {itemize}
		\item beaucoup de synchronisations
		    \implique threads \emph{user-level} pénalisés
	    \end {itemize}

    \end {itemize}

\end {frame}

\begin {frame} {Threads -- Implémentation}
    Les deux types d'implémentation extrêmes peuvent être combinés :

    \ctableau {\fD} {|c|p{.7\textwidth}|} {
	\tcol{User:Kernel}{|} & \tcol{Description}{} \\ \hline
	1:1 & \emph{Kernel-level threads} : chaque entité d'exécution
	    est supportée par le noyau
	    \\
	N:1 & \emph{User-level threads} : tous les threads sont gérés
	    en mode utilisateur, le noyau ne connaît que le processus
	    \\
	M:N & modèle hybride (M $>$ N) : M threads sont gérés
	    en mode utilisateur, et reposent sur $N$ threads gérés par le
	    noyau
	    \\
    }

    Mode hybride :

    \begin {itemize}
	\item Avantage : concevoir l'application pour bénéficier des
	    performances des threads en mode utilisateur

	\item Inconvénient : plus complexe à implémenter

    \end {itemize}

\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Threads POSIX
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Threads POSIX}

\begin {frame} {Threads POSIX}
    Norme POSIX :
    \begin {itemize}
	\item À l'origine, Unix ne disposait que des processus
	    \begin {itemize}
		\item 1988 : POSIX a normalisé les primitives \emph{existantes}
	    \end {itemize}
	\item fin des années 1980 : apparition du concept de threads
	    \begin {itemize}
		\item implémentations expérimentales
		\item divergence des API
	    \end {itemize}
	\item POSIX 1995 : introduction des threads POSIX
	    \begin {itemize}
		\item interface créée ex-nihilo
		\item principes différents des primitives
		    \begin {itemize}
			\item code retour = numéro d'erreur ou 0,
			    absence de variable \code{errno}
		    \end {itemize}
	    \end {itemize}
	\item interface connue également sous le nom \frquote{\emph{pthread}}
	\item interface très largement répandue aujourd'hui
    \end {itemize}
\end {frame}

\begin {frame} {Threads POSIX}
    API normalisée par POSIX

    \begin {itemize}
	\item fichier d'inclusion : \code{\#include <pthread.h>}
	\item compilation : \code{cc -pthread -c ...}
	    \hspace{10mm} \textit{(avec gcc et clang)}
	\item édition de liens : \code{cc -o ...-l pthread}
    \end {itemize}

    Types~:

    \begin {itemize}
	\item \code{pthread\_t}~: identificateur de thread
	\item \code{pthread\_attr\_t}~: attribut de thread
    \end {itemize}

    \medskip

    \alert{Attention} ! Contrairement aux primitives systèmes classiques,
    les fonctions \code{pthread\_*} :

    \begin {itemize}
	\item ne modifient pas \code{errno}
	\item renvoient 0 en cas de succès ou un numéro d'erreur ($> 0$) sinon
    \end {itemize}

\end {frame}

\begin {frame} [fragile] {Threads POSIX}
    Fonctions de l'API~:

    \begin {itemize}
	\item création de thread~:
\begin {lstlisting} [basicstyle=\fB\lstmonstyle]
int pthread_create (pthread_t *id,
		const pthread_attr_t *attr,
		void *(*fonction) (void *),
		void *arg)
\end{lstlisting}

	\item terminaison de thread~:

	    \code{void pthread\_exit (void *valretour)}

	\item attente de terminaison de thread~:

	    \code{int pthread\_join (pthread\_t id, void **valretour)}

    \end {itemize}

\end{frame}

\begin {frame} {Threads POSIX}
    Illustration~: 4 threads (principal + 3 nouveaux)
    \begin {center}
	\fig {thr-api} {.9}
    \end {center}
\end {frame}

\begin {frame} {Threads POSIX}
    \lst {thr.c} {\fD} {, firstline=8, lastline=22}
\end {frame}

\begin {frame} {Threads POSIX}
    \lst {thr.c} {\fD} {, firstline=33, lastline=56}
\end {frame}

\begin {frame} {Threads POSIX}
    La fonction de démarrage d'un thread a un et un seul argument, de 
    type \frquote{\code{void *}} forcément :

    \begin {itemize}
	\item c'est comme ça...
	    \begin {itemize}
		\item même si on n'a aucun argument
		\item même si on a plus d'un argument
	    \end {itemize}
	\item \frquote{\code{void *}} permet n'importe quel passage
	    d'arguments
	    \begin {itemize}
		\item plusieurs arguments
		    \implique les empaqueter dans une structure

		\lst {thr-narg.c} {\fE} {}

	    \end {itemize}

    \end {itemize}
\end {frame}

\begin {frame} {Fonctions \frquote{thread-safe}}
    La plupart des fonctions de bibliothèque ont été créées avant
    l'apparition des threads

    \begin {itemize}
	\item La plupart n'ont pas nécessité de modifications

	    Exemples~: \code{strlen}, \code{isalpha}, etc.

	\item Pour certaines, l'implémentation a dû être modifiée

	    \implique ajout de synchronisations
	    (cf chapitre suivant)

	\item Quelques unes \textbf {ne peuvent pas} être compatibles
	    avec les threads :

	    \begin {itemize}
		\item stockage d'un état dans une variable unique
		\item retour d'une adresse pointant dans une variable unique
	    \end {itemize}

	    \implique problème \alert{intrinsèque de conception} des fonctions

    \end {itemize}

\end {frame}

\begin {frame} {Fonctions \frquote{thread-safe} -- État global}
    
    Stockage d'un état dans une variable unique~:

    \begin {itemize}
	\item Exemple (découpage d'une chaîne en tokens)~:

	    \lst {strtok.c} {\fD} {}

	\item L'état actuel du parcours dans \code{str} est mémorisé
	    dans \code{last}

	    \begin {itemize}
		\item Si deux threads utilisent \code{strtok} sur
		    des chaînes différentes, la variable \code{last}
		    renvoie à l'un des threads un pointeur dans la
		    chaîne de l'autre thread

	    \end {itemize}


	\item Problème inhérent à la définition de \code{strtok}

    \end {itemize}
\end {frame}

\begin {frame} {Fonctions \frquote{thread-safe} -- État global}

    \begin {itemize}
	\item Définition d'une nouvelle fonction

	\item \code{\fB char *strtok\_r (char *s, char *delim, char **parcours)}

	\item La variable \code{last} est \frquote{sortie} de
	    \code{strtok} via \code{parcours}

    \end {itemize}

\end {frame}

%\begin {frame} {Threads}
%    \begin {itemize}
%	\item ex: \code{int rand (void)}
%
%	    Génère un nombre pseudo-aléatoire $u_{n+1} = f(u_n)$
%
%	    L'état $u_n$ est stocké dans une variable globale
%
%	    \begin {itemize}
%		\item 2 threads différents peuvent calculer la même
%		    valeur $u_{n+1}$
%		\item risque de corrompre l'état global $u_n$
%	    \end {itemize}
%
%    \end {itemize}
%\end {frame}


\begin {frame} {Fonctions \frquote{thread-safe} -- Retour global}

    Retour d'adresse pointant dans une variable unique :

    \begin {itemize}

	\item Exemple~:

	    \lst {ctime.c} {\fD} {}

	    \begin {itemize}
		\item \code{ctime} convertit un nombre de secondes depuis
		    \emph{Epoch} en une chaîne de 26 caractères :
		    \\
		    \hspace* {15mm}
		    \texttt{Sun Sep 16 01:03:52 1973{\textbackslash}n\textbackslash0}

		\item Si deux threads utilisent \code{ctime}, la
		    valeur calculée par le deuxième thread remplace
		    la valeur que le premier thread n'a peut être pas
		    encore exploitée

		\item \code{ctime} ne peut pas faire autrement~:
		    sa spécification ne permet pas d'allouer ou de
		    libérer la mémoire utilisée

	    \end {itemize}

	\item Problème inhérent à la définition de \code{ctime}

    \end {itemize}
\end {frame}

\begin {frame} {Fonctions \frquote{thread-safe} -- Retour global}

    \begin {itemize}
	\item Définition d'une nouvelle fonction :

	    \code{\fB char *ctime\_r (const time\_t *t, char *buf)}

	\item Le résultat est placé dans une zone de mémoire allouée
	    par l'appelant \\
	    \implique adresse précisée par \code{buf}

    \end {itemize}

\end {frame}

\begin {frame} {Fonctions \frquote{thread-safe}}
    Attention donc aux fonctions qui ne sont pas \frquote{thread-safe}~:

    \begin {itemize}
	\item \code{\fB rand} \implique \code{\fB rand\_r}
	\item \code{\fB getlogin} \implique \code{\fB getlogin\_r}
	\item \code{\fB ttyname} \implique \code{\fB ttyname\_r}
	\item etc.
    \end {itemize}

    \medskip

    Le manuel est votre ami : le manuel d'une fonction \texttt {x}
    mentionne également la fonction \texttt {x\_r} si elle existe

    \bigskip

    Exception pour \code{readdir\_t} : obsolète, il faut utiliser
    \code{readdir} à la place
\end {frame}

\begin {frame} {Fonctions \frquote{thread-safe} -- Variable errno}
    Et la variable \code{errno}~?

    \begin {itemize}
	\item Avant les threads, \code{errno} était une variable
	    globale

	\item Avec les threads, dans les premiers temps de POSIX,
	    \code{errno} était toujours une variable globale

	    \begin {itemize}
		\item l'erreur d'un thread pouvait \frquote{écraser}
		    l'erreur d'un autre thread
		\item comportement non \frquote{thread-safe}
	    \end {itemize}

	\item Avec les dernières versions POSIX, \code{errno}
	    devient une variable propre à chaque thread

	    \smallskip

	    \implique il est donc dorénavant possible d'écrire~:

	    \lst {errno.c} {\fC} {}

    \end {itemize}
\end {frame}


\begin {frame} {Attributs de threads}
    Les threads peuvent avoir des attributs \\
    \implique positionnés lors de la création du thread

    \begin {itemize}
	\item \code{int pthread\_attr\_init (pthread\_attr\_t *attr)}
	\item \code{int pthread\_attr\_destroy (pthread\_attr\_t *attr)}
    \end {itemize}

    \bigskip

    Le type \code{pthread\_attr\_t}~:
    \begin {itemize}
	\item est opaque
	\item peut référencer plusieurs attributs
	\item est ensuite passé à \code{pthread\_create}
    \end {itemize}
\end {frame}

\begin {frame} {Attributs de threads}

    Exemple~:

    \lst {thr-attr.c} {\fC} {}
\end {frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Threads et signaux
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Threads et signaux}

\begin {frame} {Threads et signaux}

    Les threads d'un processus partagent~:
    \begin {itemize}
	\item l'action associée à chaque signal \\
	    \code{int sigaction (int signum, const struct sigaction *new,
	                         struct sigaction *old)}
    \end {itemize}

    Chaque thread a en propre~:
    \begin {itemize}
	\item son masque de signaux (hérité du masque du processus)


	    \code{int pthread\_sigmask (int comment, const sigset\_t *new, sigset\_t *old)}

	\item les signaux en attente de réception

    \end {itemize}
\end {frame}

\begin {frame} {Threads et signaux}

    Un signal peut être envoyé à un thread en particulier~:

    \begin {itemize}
	\item à un thread désigné \\
	    \code{int pthread\_kill (pthread\_t id, int sig)}
	\item au thread fautif \\
	    Exemple : SIGSEGV en cas de problème de pointeur
    \end {itemize}

    \bigskip

    Si un signal n'est pas dirigé vers un thread (\code{kill()}
    ou événement externe comme SIGINT), il est envoyé à un thread
    arbitraire~:

    \begin {itemize}
	\item à l'un des threads qui attendent le signal \\
	    \code{int sigwait (const sigset\_t *set, int *sig)}
	\item à l'un des threads qui autorisent le signal
    \end {itemize}

\end {frame}

\begin {frame} {Threads et signaux}
    \begin {center}
	\fig {thr-sig} {.9}
    \end {center}

\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Barrières
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Barrières}

\begin {frame} {Barrières}
    Les threads peuvent utiliser de nombreux mécanismes de
    synchronisation~:
    \begin {itemize}
	\item verrous
	\item variables de condition
	\item barrières
	\item etc.
    \end {itemize}

    Commençons par un mécanisme simple~: la barrière

\end {frame}

\begin {frame} {Barrières}
    Principe~:

    \begin {itemize}
	\item une barrière de $n$ threads est bloquante pour les $n-1$
	    premiers threads

	\item lorsque le $n$-ème thread se présente à la barrière,
	    les $n$ threads sont libérés

	\item la barrière est alors de nouveau prête à fonctionner

    \end {itemize}
\end {frame}

\begin {frame} {Barrières}

    \begin {center}
	\fig {bar-intro} {.6}
    \end {center}

    Valeur de retour de \code{pthread\_barrier\_wait}~:
    \begin {itemize}
	\item \code{PTHREAD\_BARRIER\_SERIAL\_THREAD} pour l'un des $n$
	    threads
	\item \code{0} pour les $n-1$ autres threads.
    \end {itemize}

\end {frame}

\begin {frame} {Barrières -- Attributs}

    Les mécanismes de synchronisation peuvent aussi avoir des
    attributs~:

    \begin {itemize}
	\item \code{\fB int pthread\_barrierattr\_init
	    (pthread\_barrierattr\_t *attr)}

	\item \code{\fB int pthread\_barrierattr\_destroy
		    (pthread\_barrierattr\_t *attr)}

    \end {itemize}

    Exemple~: barrière partagée entre plusieurs processus ou restreinte
    aux threads du processus courant

    \begin {itemize}
	\item 
		\code{pthread\_barrierattr\_setpshared ()} \\
		\code{pthread\_barrierattr\_getpshared ()}

	\item valeur \code{PTHREAD\_PROCESS\_SHARED} ou
	    \code{PTHREAD\_PROCESS\_PRIVATE}

    \end {itemize}

    Si la barrière est partagée entre processus, il faut qu'elle soit
    en mémoire partagée.

\end {frame}

\begin {frame} {Barrières -- Attributs}
    Exemple~:

    \lst {bar-attr.c} {\fC} {}
\end {frame}
