% Pour vérifier qu'on écrit du LaTeX moderne
% \RequirePackage [orthodox] {nag}

\documentclass [twoside,openright,a4paper,11pt,french] {report}

    % Règles de typographie françaises
    \usepackage[francais]{babel}

    % Jeu de caractères UTF-8
    \usepackage[utf8]{inputenc}
    \usepackage[T1]{fontenc}

    % Inclure la bibliographie dans la table des matières comme une section
    \usepackage [numbib] {tocbibind}	% numbib : section numérotée

    % Fonte élégante
    \usepackage {mathpazo}
    \usepackage [scaled] {helvet}
    \usepackage {courier}

    % pour \EUR
    \usepackage {marvosym}

    % \usepackage {emptypage}

    % Utilisation de tableaux
    \usepackage {tabularx}

    % Utilisation d'url
    \usepackage {hyperref}
    \urlstyle {sf}

    % Utilisation d'images
    \usepackage{graphicx}
    \setkeys {Gin} {keepaspectratio}	% par défaut : conserver les proportions

    % Définition des marges
    \usepackage [margin=25mm, foot=15mm] {geometry}

    \parskip=2mm
    \parindent=0mm

    \pagestyle {plain}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Page de garde
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\thispagestyle{empty}

\begin{center}
    % le ".1" est juste là pour montrer qu'on peut mettre des
    % dimensions non entières...
    \includegraphics [width=8.1cm] {logo-ufr.pdf}       

    \vfill\vfill

    {
	\large
	\textsc {
	    Licencemaster 4 de Science, mention Informatique \\
	    spécialité Réseaux Informatiques et Systèmes Embarqués
	}
    }

    \bigskip\bigskip

    {\large Mémoire de stage présenté par}

    \medskip

    % Identité de l'auteur
    {\large Jacques \textsc {Sélère}}

    % Contact mail ou téléphone   
    {\small jselere@unistra.fr}

    \vfill

    % Titre du stage : mettez un titre utile
    {
	\huge
	\textsc {
	    Comment faire un bon \\
	    ~ \\
	    mémoire de stage ?
	}
    }

    \vfill

    {\large Stage encadré par}

    \medskip

    % Identité de l'encadrant
    {\large Jean \textsc {Breille}}

    % Contact mail ou téléphone
    {\small +33 (0) 3.68.85.98.76}

    \bigskip

    {\large Au sein de}

    \medskip

    % Structure d'accueil
    {
	\large
	\textsc {Société de propulsion aéroportée des escargots}
    }

    \bigskip
    \bigskip

    % Logo de votre structure d'accueil
    \includegraphics [height=2.5cm] {logo-entreprise.pdf}       

    % Supprimez le reste de cette page si vous vous servez de ce
    % fichier source comme modèle
    \vfill
    \begin {center}
	\tiny
	\copyright Pierre David,
	avec des contributions de 
	Cristel Pelsser, Stéphane Cateloin et Vincent Loechner

	Disponible sur \url {http://gitlab.com/pdagog/ens}.

        Ce texte est placé sous licence « Creative Commons Attribution
	-- Pas d’Utilisation Commerciale 4.0 International » \\
	Pour accéder à une copie de cette licence,
	merci de vous rendre à l'adresse suivante
	\url {http://creativecommons.org/licenses/by-nc/4.0/}

	\includegraphics [scale=.5] {by-nc}
    \end {center}
    % Supprimez jusqu'ici


\end{center}

% Page blanche au dos de la page de garde
%\cleardoublepage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Table des matières
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

{
    \parskip=0pt
    \tableofcontents
}

% Page blanche entre la table des matières et le texte
\cleardoublepage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Chapitre 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter {Introduction}
    \label {chap:intro}

Le mémoire est un élément essentiel de votre stage : il a pour objet
d'exposer, le plus fidèlement possible, à la fois le périmètre du
stage (périmètres organisationnel et/ou technique) et votre contribution.

Votre mémoire sera lu par un «~rapporteur~», c'est-à-dire un
enseignant de l'équipe pédagogique (donc une personne ayant des
compétences dans votre discipline), dont la mission est d'évaluer votre
mémoire pour comprendre le contexte dans lequel vous avez évolué,
et votre contribution (réalisation technique, travail scientifique ou
autre) ainsi que son adéquation vis-à-vis de la formation.

Il est rappelé que le plagiat est sanctionné par la loi, par
l'université et par vos rapporteurs : si les courtes citations sont
autorisées, vous devez en donner la source.

Ce document a pour but de rappeler les éléments attendus par le
rapporteur pour évaluer votre travail. Il est de votre intérêt de
fournir tous ces éléments.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Chapitre 2 : le plan
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter {Le plan}
    \label {chap:plan}

Votre mémoire doit être bien structuré, c'est-à-dire présenter une
progression logique et apparente. Le plan est de votre responsabilité,
mais il doit comporter des chapitres relativement équilibrés (en taille)
et contenir les éléments décrits ci-après (un élément n'est pas
forcément un chapitre en soi). Les enchaînements entre les chapitres
et les sections doivent être compréhensibles.

N'omettez pas la table des matières, et numérotez vos chapitres et
vos sections (1, 1.1, 1.1.1, etc.), cela aide à se repérer. Et bien
évidemment, numérotez les pages, sinon ça ne sert à rien.

\section {L'organisme d'accueil}

L'exercice consiste à présenter une description de votre contexte
organisationnel : votre rapporteur ne connaît à priori pas
l'organisme d'accueil, donc vous devez le présenter à travers sa
branche d'activité, ses chiffres caractéristiques. De plus, vous
devez présenter votre place dans l'organisme : vous devez suivre une
description en «~entonnoir~» (voir figure~\ref {fig:entonnoir}),
c'est-à-dire partir de l'organisme dans son ensemble pour arriver
jusqu'à votre place en tant que stagiaire. Un organigramme peut aider
à comprendre votre place dans l'organisme ou le service concerné.

\begin {figure} [htbp]
    \begin {center}
	\includegraphics [width=.35\textwidth] {entonnoir.pdf}
    \end {center}
    \label {fig:entonnoir}
    \caption {Description de l'organisme : du plus large jusqu'à vous.}
\end {figure}

La figure~\ref {fig:entonnoir} présente la démarche : évoquer le
groupe au plus haut niveau (ses activités, son chiffre d'affaires, sa
présence dans le monde, le nombre d'employés, etc.), puis présenter
la filiale française et ainsi de suite jusqu'à arriver à l'équipe
dans laquelle vous vous situez (en précisant ses missions, le nombre
de personnes, etc.) et votre place dans cette équipe.

Évitez de recopier un site Web : cela se voit et ça énerve souvent
le rapporteur. Rédigez un texte avec vos propres mots : même s'il y a
des maladresses, cela passera beaucoup mieux.

Notez que la présentation de la structure d'accueil est obligatoire,
même si vous savez que cette structure est bien connue de votre
rapporteur (par exemple pour un stage réalisé dans le laboratoire
ICube). L'exercice imposé est de présenter cette structure, et tous
les stagiaires sont évalués sur ce critère, sans exception.

\section {La ou les missions}
    \label {sec:mission}

En tant que stagiaire, vous avez une ou plusieurs missions définies
correspondant à la durée de votre stage. Le rapporteur ne connaît
vraisemblablement pas ces missions. Décrivez les, de préférence assez
tôt dans le mémoire, en les mettant en perspective par rapport aux
besoins de l'organisme. De plus, donnez les contraintes particulières
(financières, technique ou autres) ainsi que les objectifs à atteindre,
qui permettront d'évaluer ou non la réussite du projet.

\section {Le contexte}

Le contexte technique ou organisationnel doit être présenté.
Rappelez-vous que votre rapporteur a une bonne maîtrise des concepts
techniques, mais qu'il n'a pas connaissance des contraintes particulières
liées à votre organisme d'accueil ou à son métier.

N'abusez pas du contexte : vous devez présenter le minimum pour permettre
au rapporteur de comprendre les contraintes spécifiques de votre stage
et votre contribution. Pas plus.

Enfin, évitez le \frquote{\emph{bullshit}} : les documents de
présentation des organismes abusent souvent de phrases toutes faites qui
ne veulent rien dire comme \frquote{l'entreprise X place ses clients au
cœur de ses préoccupations} ou encore \frquote{le laboratoire Y est un
laboratoire d'excellence caractérisé par le haut niveau scientifique
de ses équipes}. On attend de vous que vous restiez factuel.


\section {Votre contribution}

C'est la principale partie de votre mémoire : vous devez présenter votre
contribution (réalisation logicielle, système informatique, état de
l'art, méthode, algorithme, évaluation, etc.) de façon synthétique,
sans sombrer dans les détails techniques mais en n'éludant pas les
points concrets qui permettent au rapporteur d'avoir la vision la plus
exacte possible de ce que vous avez mis en œuvre. Ne passez pas
les difficultés auxquelles vous avez été confronté sous silence.

Soyez précis et factuel : technologies, algorithmes ou méthodes
employés, éléments quantitatifs permettant d'apprécier l'envergure
du projet, comparatifs réalisés, etc. Si vous avez travaillé en
coopération avec d'autres personnes (votre maître de stage, d'autres
personnes ou stagiaires), indiquez avec précision votre rôle et vos
réalisations.

La méthodologie avec laquelle vous procédez est l'un des principaux
critères d'évaluation de votre travail : les problèmes doivent
être analysés, les choix effectués (ou auxquels vous avez pris part)
doivent être explicités et justifiés.

Il arrive souvent que, comme stagiaire, les choix vous soient imposés :
vous vous insérez dans un contexte qui existait avant votre arrivée,
certains choix sont faits en amont ou implicitement. Vous devez
les expliciter, et montrer que vous en maîtrisez les tenants et les
aboutissants : en d'autres termes, vous devez justifier certains choix,
même si ce ne sont pas les vôtres, pour montrer votre compréhension
des enjeux.

\section {Votre bilan}

Le stage complète votre formation : vous devez prendre du recul pour
mettre en rapport votre stage avec les compétences acquises pendant votre
formation, et présenter les compétences complémentaires (techniques,
personnelles, comportementales, etc.) que vous avez acquises durant
le stage.

Le bilan doit montrer votre recul par rapport au stage : vous avez le
droit (voire le devoir) de jeter un regard personnel et critique sur
votre comportement, sur votre démarche, sur votre réalisation ou même
sur le contexte, les choix effectués ou l'organisme d'accueil.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Chapitre 3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter {La forme}
    \label {chap:contexte}

Un bon mémoire allie un fond de qualité et une forme parfaite. Quelques
règles de bon sens s'appliquent.

\section {Typographie}

La rédaction de textes français obéit à des règles précises en
Français : cela s'appelle la «~typographie~» \cite{andre1990} et il
est intéressant de s'en imprégner pour éviter des erreurs grossières
et donner à votre document un aspect de qualité.

\section {L'orthographe et la grammaire}

L'orthographe et la grammaire sont des prérequis indispensables pour
la rédaction du mémoire. Si vous n'êtes pas sûr de vous, faites-vous
relire par un tiers. C'est dommage de perdre des points sur ce critère.

\section {Le style}

Même si votre mémoire de stage n'a pas comme objectif de décrocher
le prix Goncourt, vous devez faire attention au style :

\begin {itemize}
    \item faites des phrases construites, avec des verbes. Exemple vécu,
	à éviter : «~Il y a des problèmes. Par exemple le
	format~» (il n'y pas de verbe)~;

    \item ne parlez pas au lecteur. Exemple vécu, à éviter : «~je vais
	vous présenter~»~;

    \item employez une forme active : plutôt qu'écrire «~xyz a été
	réalisé~», utilisez «~j'ai réalisé xyz~»~;

    \item soyez précis : écrivez «~j'ai réalisé xyz~» plutôt que «~on a
	réalisé xyz~». N'oubliez pas que le rapporteur doit évaluer
	\textbf{votre} travail (et pas celui de votre maître de stage ou
	de vos collègues)~;

    \item évitez le futur : tout ce qui est fait au moment de l'écriture
	du mémoire doit être rédigé au passé ou au
	présent. Réservez le futur pour ce qui n'est pas encore
	réalisé ou pour les perspectives.

\end {itemize}


\section {Numérotez}

Numérotez tout ce qui peut l'être : pages, chapitres, sections, figures,
tables, bibliographie. Laissez à votre logiciel le soin de numéroter
automatiquement, il le fera mieux que vous «~manuellement~». Utilisez
des références si vous devez mettre en relation plusieurs éléments
de votre discours (exemple fictif : voir la figure~\ref {fig:entonnoir}
page~\pageref {fig:entonnoir}, ou encore le chapitre~\ref {chap:plan}
et plus spécifiquement la section~\ref {sec:mission}, page~\pageref
{sec:mission}).


\section {Les illustrations}

Un petit dessin valant mieux qu'un grand discours, les schémas de
principe sont souvent appréciés par le rapporteur, qui ne connaît
pas votre environnement. Quelques règles cependant sont à respecter :

\begin {itemize}
    \item numérotez et légendez vos figures (voir figure~\ref
	{fig:entonnoir}, page~\pageref {fig:entonnoir}, par exemple) ;
    \item référencez vos figures : une figure non référencée dans le
	texte ne sert à rien ;
    \item expliquez vos figures dans le texte : si une figure n'a pas
	besoin d'explication, cela signifie qu'elle est sans doute trop
	simple et n'a donc pas besoin de figurer dans votre mémoire ;
    \item citez la provenance de vos figures, si elles ne sont pas de
	vous : il est parfaitement admis d'utiliser des figures
	réalisées par d'autres, si vous citez la provenance ;
    \item les logos de logiciels ou de produits sont à bannir : ils
	n'apportent rien à la compréhension de votre mémoire et ne font
	que prendre de la place inutile que vous pourriez utiliser pour
	mieux présenter votre contribution ;
    \item privilégiez (sauf peut-être pour les photos) un format
	«~vectoriel~» à un format «~bitmap~» : le bitmap prend
	de la place, peut être lent à s'afficher ou à s'imprimer,
	et ne permet pas de zoomer facilement ;
    \item vérifiez que vos figures sont lisibles lorsque vous imprimez
	votre mémoire, y compris lorsque vous imprimez sur une
	imprimante noir et blanc.
\end {itemize}

Les copies d'écran n'apportent généralement pas grand-chose : elles
contiennent trop d'informations inutiles et sont peu lisibles. De plus,
le message véhiculé est souvent très succinct, voire trop léger. Un
fichier de configuration, une commande ou son résultat doivent être
présentés, si c'est vraiment nécessaire, comme du texte et non comme
une copie d'écran.

\section {Le niveau de détail}

Trouver le bon niveau de détail est souvent facilité par les contraintes
qui vous sont imposées quant au nombre de pages.

Cependant, même si vous avez la place, évitez les recopies de code ou de
commandes. Si vous voulez vraiment en faire, commentez-les dans le texte
et évitez de faire des erreurs dans les recopies ! Ne les modifiez pas
dans le texte (sauf pour supprimer une sortie trop longue par exemple,
dans ce cas remplacez la partie supprimée par «~[...]~»). Évitez
les codes trop longs~: utilisez des annexes si nécessaire.


\section {La bibliographie}

La bibliographie constitue une partie importante de votre mémoire.
Elle constitue un critère de qualité du travail (avez-vous trouvé les
bonnes sources ? les documents sur lesquels vous vous appuyez sont-ils
sérieux ?). Vous devez indiquer les documents :

\begin {itemize}
    \item de référence que vous avez consultés dans votre recherche,
	pour vous familiariser avec votre sujet ou pour apprendre
	des techniques particulières ;
    \item que vous avez consultés pour effectuer vos choix ou mettre
	en œuvre un dispositif logiciel ou autre ;
    \item qui permettent au lecteur d'en savoir plus sur tel ou tel
	point de votre mémoire que vous ne pouvez davantage développer.
\end {itemize}

La bibliographie~\cite {savoirs2010} vient en annexe, elle doit donner
tous les renseignements nécessaires pour permettre au lecteur de
retrouver les documents concernés : auteur, titre du document ou de
l'ouvrage, éditeur, année de publication, URL si nécessaire, date de
consultation pour un site Web, etc.

Chaque document dans la bibliographie comporte une référence (un
numéro, une abréviation ou autre), que vous devez citer dans le texte :
un document non cité ne devrait pas apparaître dans la bibliographie.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Conclusion
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter {Conclusion}
    \label {chap:conc}

Il est maintenant temps de rédiger votre mémoire. Puissent ces quelques
conseils vous guider afin de vous aider à présenter le mieux possible
tout le travail que vous avez effectué durant votre stage !


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Annexe : soutenance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\appendix
\chapter {La soutenance}

Même si ce document concerne en priorité votre mémoire de stage,
il n'est pas inutile de rappeler quelques conseil de bon sens
pour que votre soutenance se déroule le mieux possible.

\begin {enumerate}
    \item Ne reproduisez pas le mémoire dans votre présentation : vous
	n'avez ni la place, ni le temps. Détachez-vous du mémoire et
	repartez de zéro pour construire un nouveau discours tenant
	compte de la contrainte de temps.

    \item Travaillez sur les idées et les messages que vous voulez
	faire passer. Comptez une idée par diapo. Explicitez les idées,
	ne vous contentez pas de les suggérer.

    \item Ne surchargez pas le texte de votre présentation : ne faites
	pas de phrases, insistez plutôt sur quelques mots pour exposer
	vos idées.

    \item Si vous pouvez prendre des libertés avec la grammaire et
	ne pas mettre de phrases, vous n'êtes pas dispensé de respecter
	l'orthographe.

    \item Adoptez un fond sobre pour ne pas perturber votre message.

    \item Ne lisez surtout pas les diapos que vous présentez ou, pire
	encore, un texte que vous auriez préparé.

    \item Faites des illustrations (schémas, figures, courbes) qui
	puissent être lues à plusieurs mètres de distance. N'hésitez
	pas à prendre des libertés avec votre style de présentation
	pour faire une figure en pleine page.

    \item Attention aux contrastes : votre présentation projetée dans
	une salle éclairée aura un contraste beaucoup moins bon que
	l'écran de votre portable. Évitez donc les couleurs pâles
	sur fond clair, ou les couleurs peu foncées sur fond sombre.

    \item Une de vos missions est de maintenir l'attention de votre
	auditoire. Pensez que les membres du jury ont déjà peut-être
	une dizaine de présentations à leur actif, ainsi qu'un bon
	repas...  Vous devez les motiver pour vous écouter.

    \item Respectez la durée de votre présentation. Ne terminez pas
	trop en avance (vous n'avez donc rien à dire ?), ne terminez pas
	trop en retard (vous ne savez pas synthétiser et tenir compte
	d'une contrainte ?).

    \item Répétez. Répétez. Répétez. Répétez. Répétez. Répétez.
	Répétez. Répétez. Répétez. Répétez. Répétez. Répétez.

    \item Lors de la séance des questions, laissez les membres du jury
	aller jusqu'au bout de leurs questions, sans les
	interrompre. N'hésitez pas à prendre quelques secondes pour
	vous permettre de réfléchir à chaque question, voire de la
	reformuler pour vérifier que vous l'avez bien comprise.

\end {enumerate}

Si vous n'avez pas l'habitude de faire des présentations, vous trouverez
facilement un grand nombre de vidéos ou de tutoriels qui vous donneront
de bons conseils.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bibliographie
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\cleardoublepage
\bibliographystyle{plain}
\bibliography{memoire}

\end{document}
